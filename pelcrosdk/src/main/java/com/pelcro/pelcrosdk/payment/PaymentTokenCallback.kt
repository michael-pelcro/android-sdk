package com.pelcro.pelcrosdk.payment

interface PaymentTokenCallback {
    fun onSuccess(stripeToken: String)
    fun onError(message: String?) {}
    fun onCancelled() {}
}