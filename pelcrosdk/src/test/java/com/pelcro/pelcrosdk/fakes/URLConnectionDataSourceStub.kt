package com.pelcro.pelcrosdk.fakes

import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.models.HTTPMethod
import java.net.URL

class URLConnectionDataSourceStub: URLConnectionDataSource {

    var urlToReturn = URL("https://pelcro.com")
    val testTimeoutValue = 20
    var testHttpMethod = HTTPMethod.GET
    var testHeaders = hashMapOf(Pair("key1", "value1"), Pair("key2", "value2"))
    var testParams: ByteArray? = ByteArray(1)

    override fun getUrl(): URL = urlToReturn

    override fun getRequestMethod(): HTTPMethod {
        return testHttpMethod
    }

    override fun getTimeout(): Int {
        return testTimeoutValue
    }

    override fun getHeaders(): Map<String, String> {
        return testHeaders
    }

    override fun getParams(): ByteArray? {
        return testParams
    }

}