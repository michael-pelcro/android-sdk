package com.pelcro.pelcrosdk.data.providers

import android.os.AsyncTask
import com.pelcro.pelcrosdk.data.abstraction.DataResultHandler
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.data.models.DataErrorType
import com.pelcro.pelcrosdk.general.model.Result
import com.pelcro.pelcrosdk.general.model.Success
import com.pelcro.pelcrosdk.general.model.Error
import java.io.InputStream
import java.net.HttpURLConnection.HTTP_OK
import javax.net.ssl.HttpsURLConnection

internal class AsyncHttpsURLConnector
    (private val resultHandler: DataResultHandler<InputStream, InputStream>)
    : AsyncTask<HttpsURLConnection, Int, Result<InputStream, DataError<InputStream>>>() {

    override fun doInBackground(vararg connections: HttpsURLConnection?): Result<InputStream, DataError<InputStream>> {
        val connection = connections[0]
        connection?.connect()
        val responseCode = connection?.responseCode
        if (responseCode == HTTP_OK) {
            return Success(connection.inputStream)
        }
        val errorMessage = "HTTP error with code:$responseCode"
        val error = DataError(DataErrorType.RESPONSE_UNACCEPTABLE, errorMessage, connection?.errorStream)
        return Error(error)
    }

    override fun onPostExecute(result: Result<InputStream, DataError<InputStream>>?) {
        resultHandler.onResult(result!!)
    }
}