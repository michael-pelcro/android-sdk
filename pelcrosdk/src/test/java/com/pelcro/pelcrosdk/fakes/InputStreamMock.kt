package com.pelcro.pelcrosdk.fakes

import java.io.InputStream

internal class InputStreamMock: InputStream() {
    override fun read(): Int {
        return 0
    }
}