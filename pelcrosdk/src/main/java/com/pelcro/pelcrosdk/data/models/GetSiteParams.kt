package com.pelcro.pelcrosdk.data.models

internal data class GetSiteParams(
    val siteID: String
)