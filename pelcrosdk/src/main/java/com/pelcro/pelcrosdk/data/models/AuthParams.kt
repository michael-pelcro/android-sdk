package com.pelcro.pelcrosdk.data.models

internal data class AuthParams(
    val siteID: String,
    val email: String,
    val password: String
)