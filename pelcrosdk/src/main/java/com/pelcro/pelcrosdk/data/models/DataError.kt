package com.pelcro.pelcrosdk.data.models

internal data class DataError<T>(val type: DataErrorType, val message: String, val rawErrorData: T?)