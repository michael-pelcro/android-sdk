package com.pelcro.pelcrosdk

import com.pelcro.pelcrosdk.data.AuthConnectionDataSourceFactory
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.models.*
import com.pelcro.pelcrosdk.fakes.AuthParamsConverterMock
import com.pelcro.pelcrosdk.fakes.BaseURLProviderStub
import org.junit.Test
import org.junit.Assert.*

class AuthConnectionDataSourceFactoryTests {

    private val baseURLProviderStub = BaseURLProviderStub()
    private val paramsConverterMock = AuthParamsConverterMock()
    private val sut = AuthConnectionDataSourceFactory(baseURLProviderStub, paramsConverterMock)

    @Test
    fun `Return data source with 30 seconds timeout`() {
        assertEquals(30*1000, testLoginDataSource().getTimeout())
    }

    @Test
    fun `when creating get site info data source with site id, the url is correct`() {
        assertEquals(urlWith("site?site_id=$testsiteID"),
            testSiteInfoDataSource().getUrl().toExternalForm())
    }

    @Test
    fun `when creating get site info data source, set appropriate http method`() {
        assertEquals(HTTPMethod.GET, testSiteInfoDataSource().getRequestMethod())
    }

    @Test
    fun `when creating get site info data source, return no header values`() {
        assertEquals(0, testSiteInfoDataSource().getHeaders().entries.size)
    }

    @Test
    fun `when creating get site info data source, don't convert params`() {
        testSiteInfoDataSource()
        assertNull(paramsConverterMock.receivedData)
    }

    @Test
    fun `when creating get site info data source, set no params`() {
        assertNull(testSiteInfoDataSource().getParams())
    }

    //Register data source tests
    @Test
    fun `when creating register data source, return correct url`() {
        assertEquals(urlWith("auth/register"), testRegisterDataSource().getUrl().toExternalForm())
    }

    @Test
    fun `when creating register data source, return POST http method`() {
        assertEquals(HTTPMethod.POST, testRegisterDataSource().getRequestMethod())
    }

    @Test
    fun `when creating register data source, return correct Accept header`() {
        assertEquals("application/json", testRegisterDataSource().getHeaders()["Accept"])
    }

    @Test
    fun `when creating register data source, return correct Content-Type header`() {
        assertEquals("application/json", testRegisterDataSource().getHeaders()["Content-Type"])
    }

    @Test
    fun `when creating register data source, convert params`() {
        testRegisterDataSource()
        assertEquals(testRegisterParams, paramsConverterMock.receivedData)
    }

    @Test
    fun `when creating register data source, return converted params`() {
        assertEquals(testRegisterDataSource().getParams(), paramsConverterMock.testConvertedData)
    }

    //Login data source tests
    @Test
    fun `when creating login data source, return correct url`() {
        assertEquals(urlWith("auth/login"), testLoginDataSource().getUrl().toExternalForm())
    }

    @Test
    fun `when creating login data source, return POST http method`() {
        assertEquals(HTTPMethod.POST, testLoginDataSource().getRequestMethod())
    }

    @Test
    fun `when creating login data source, return correct Accept header`() {
        assertEquals("application/json", testLoginDataSource().getHeaders()["Accept"])
    }

    @Test
    fun `when creating login data source, return correct Content-Type header`() {
        assertEquals("application/json", testRegisterDataSource().getHeaders()["Content-Type"])
    }

    @Test
    fun `when creating login data source, convert params`() {
        testLoginDataSource()
        assertEquals(testLoginParams, paramsConverterMock.receivedData)
    }

    @Test
    fun `when creating login data source, return converted params`() {
        assertEquals(testLoginDataSource().getParams(), paramsConverterMock.testConvertedData)
    }

    //Helpers
    private fun urlWith(suffix: String): String {
        val baseURL = baseURLProviderStub.data
        return "$baseURL/$suffix"
    }

    private fun testSiteInfoDataSource(): URLConnectionDataSource {
        return sut.requestDataSourceFor(AuthRequestType.GET_SITE_INFO, testSiteInfoParams)
    }

    private fun testRegisterDataSource(): URLConnectionDataSource {
        return sut.requestDataSourceFor(AuthRequestType.REGISTER, testRegisterParams)
    }

    private fun testLoginDataSource(): URLConnectionDataSource {
        return sut.requestDataSourceFor(AuthRequestType.LOGIN, testLoginParams)
    }

    private companion object {
        private const val testsiteID = "123asd123"
        private const val testLanguage = "es"
        private const val testAccountID = "987asd08"
        private const val testEmail = "test-email"
        private const val testPassword = "myTestPassword"
        private val testLoginAuthParams = AuthParams(testsiteID, testEmail, testPassword)
        private val testRegisterAuthParams = RegisterParams(testLoginAuthParams, testAccountID, testLanguage)
        private val testSiteInfoParams = AuthParamsType.GetSiteInfo(testsiteID)
        private val testLoginParams = AuthParamsType.Login(testLoginAuthParams)
        private val testRegisterParams = AuthParamsType.Register(testRegisterAuthParams)
    }
}