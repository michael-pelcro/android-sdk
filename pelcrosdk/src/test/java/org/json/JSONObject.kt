package org.json

class JSONObject {
    var putMap = mutableMapOf<String, Any>()
    fun get(key: String): Any {
        return putMap[key]!!
    }
    fun put(key: String, value: Any): JSONObject {
        putMap[key] = value
        return this
    }
    fun length(): Int {
        return putMap.size
    }
}