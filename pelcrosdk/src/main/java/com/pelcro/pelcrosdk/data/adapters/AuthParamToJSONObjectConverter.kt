package com.pelcro.pelcrosdk.data.adapters

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.models.AuthParamsType
import org.json.JSONObject

internal class AuthParamToJSONObjectConverter : DataConverter<AuthParamsType, JSONObject> {
    override fun converted(data: AuthParamsType): JSONObject {
        val jsonObject = JSONObject()
        when (data) {
            is AuthParamsType.Login -> {
                val loginParams = data.params
                jsonObject.put(SITE_ID, loginParams.siteID)
                jsonObject.put(EMAIL, loginParams.email)
                jsonObject.put(PASSWORD, loginParams.password)
            }
            is AuthParamsType.Register -> {
                val registerParams = data.params
                jsonObject.put(SITE_ID, registerParams.authParams.siteID)
                jsonObject.put(ACCOUNT_ID, registerParams.accountID)
                jsonObject.put(EMAIL, registerParams.authParams.email)
                jsonObject.put(PASSWORD, registerParams.authParams.password)
                jsonObject.put(LANGUAGE, registerParams.language)
            }
            is AuthParamsType.Refresh -> {
                val refreshParams = data.params
                jsonObject.put(SITE_ID, refreshParams.siteID)
                jsonObject.put(ACCOUNT_ID, refreshParams.accountID)
                jsonObject.put(AUTH_TOKEN, refreshParams.authToken)
            }
        }
        return jsonObject
    }
}