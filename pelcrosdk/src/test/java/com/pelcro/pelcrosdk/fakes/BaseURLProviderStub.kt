package com.pelcro.pelcrosdk.fakes

import com.pelcro.pelcrosdk.data.abstraction.DataProvider

class BaseURLProviderStub: DataProvider<String> {
    override var data: String = "https://pelcro.com"
}