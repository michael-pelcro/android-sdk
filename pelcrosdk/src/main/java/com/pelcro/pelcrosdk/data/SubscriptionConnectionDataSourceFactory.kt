package com.pelcro.pelcrosdk.data

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.abstraction.DataProvider
import com.pelcro.pelcrosdk.data.abstraction.RequestDataSourceFactory
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.models.HTTPMethod
import com.pelcro.pelcrosdk.data.models.SubscriptionParamsType
import com.pelcro.pelcrosdk.data.models.SubscriptionRequestType
import java.net.URL

internal class SubscriptionConnectionDataSourceFactory
    (
    private val baseURLProvider: DataProvider<String>,
    private val paramsConverter: DataConverter<SubscriptionParamsType, ByteArray>
) :
    RequestDataSourceFactory<SubscriptionRequestType, SubscriptionParamsType> {

    override fun requestDataSourceFor(
        requestType: SubscriptionRequestType,
        params: SubscriptionParamsType
    ): URLConnectionDataSource {
        val basURLString = baseURLProvider.data

        val urlString = when (requestType) {
            SubscriptionRequestType.CREATE_SUBSCRIPTION -> "$basURLString/subscription"
            SubscriptionRequestType.CANCEL_SUBSCRIPTION -> "$basURLString/subscription/cancel"
            SubscriptionRequestType.REACTIVATE_SUBSCRIPTION -> "$basURLString/subscription/reactivate"
        }
        val method = HTTPMethod.POST

        val headers = mapOf(
            Pair("Accept", "application/json"),
            Pair("Content-Type", "application/json")
        )

        val convertedParams = paramsConverter.converted(params)
        return BaseURLConnectionDataSource(URL(urlString), method, 30 * 1000, headers, convertedParams)
    }
}