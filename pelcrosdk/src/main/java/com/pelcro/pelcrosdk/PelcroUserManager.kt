package com.pelcro.pelcrosdk

import android.content.Context
import com.pelcro.pelcrosdk.data.abstraction.ResultHandler
import com.pelcro.pelcrosdk.data.abstraction.RequestDataSourceFactory
import com.pelcro.pelcrosdk.data.adapters.AUTH_TOKEN
import com.pelcro.pelcrosdk.data.models.AuthParams
import com.pelcro.pelcrosdk.data.models.AuthParamsType
import com.pelcro.pelcrosdk.data.models.AuthRequestType
import com.pelcro.pelcrosdk.data.models.PelcroResult
import com.pelcro.pelcrosdk.data.models.RefreshParams
import com.pelcro.pelcrosdk.data.models.RegisterParams
import com.pelcro.pelcrosdk.dependencyinjection.RequestSendingComponentsProvider
import com.pelcro.pelcrosdk.dependencyinjection.ManagerComponentsProvider

class PelcroUserManager {

    private val siteManager: PelcroSiteManager
    private val componentsProvider: RequestSendingComponentsProvider
    private val requestDataSource: RequestDataSourceFactory<AuthRequestType, AuthParamsType>

    constructor(context: Context) : this(ManagerComponentsProvider(context), context)

    internal constructor(
        componentsProvider: RequestSendingComponentsProvider,
        context: Context
    ) {
        this.componentsProvider = componentsProvider
        requestDataSource = componentsProvider.getAuthorizationRequestDataSourceFactory()
        siteManager = PelcroSiteManager(context)
        Pelcro.authToken = componentsProvider.getRequestDataStorage().getAuthToken()
    }

    fun registerWith(email: String, password: String, handler: ResultHandler) {
        componentsProvider.getResultHandlerInput().set(object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                saveData(result)
                handler.onResult(result)
            }
        })
        val authParams = authParamsWith(Pelcro.siteID, email, password)
        val registerParams = RegisterParams(authParams, Pelcro.accountID)
        val dataSource = requestDataSource.requestDataSourceFor(
            AuthRequestType.REGISTER,
            AuthParamsType.Register(registerParams)
        )
        componentsProvider.startNewConnectionTaskWith(dataSource)
    }

    fun loginWith(email: String, password: String, handler: ResultHandler) {
        componentsProvider.getResultHandlerInput().set(object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                saveData(result)
                handler.onResult(result)
            }
        })
        val authParams = authParamsWith(Pelcro.siteID, email, password)
        val dataSource = requestDataSource.requestDataSourceFor(
            AuthRequestType.LOGIN,
            AuthParamsType.Login(authParams)
        )
        componentsProvider.startNewConnectionTaskWith(dataSource)
    }

    fun refreshWith(handler: ResultHandler) {
        componentsProvider.getResultHandlerInput().set(object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                saveData(result)
                handler.onResult(result)
            }
        })
        val refreshParams = refreshParamsWith(Pelcro.siteID, Pelcro.accountID, Pelcro.authToken)
        val dataSource = requestDataSource.requestDataSourceFor(
            AuthRequestType.REFRESH,
            AuthParamsType.Refresh(refreshParams)
        )
        componentsProvider.startNewConnectionTaskWith(dataSource)
    }

    fun read() = Pelcro.user

    fun logout() {
        Pelcro.authToken = ""
        Pelcro.user = null
        componentsProvider.getRequestDataStorage().clearAuthToken()
    }

    private fun saveData(result: PelcroResult) {
        Pelcro.authToken = Pelcro.getDataEntry(result.data, AUTH_TOKEN)
        Pelcro.user = Pelcro.getData(result.data)
        componentsProvider.getRequestDataStorage().saveAuthToken(Pelcro.authToken)
    }

    private fun authParamsWith(siteID: String, email: String, password: String) =
        AuthParams(siteID, email, password)

    private fun refreshParamsWith(siteID: String, accountID: String, authToken: String) =
        RefreshParams(siteID, accountID, authToken)
}