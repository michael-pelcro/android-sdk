package com.pelcro.pelcrosdk.data.abstraction

internal interface DataConverter<DataToConvertType, ConvertedType> {
    fun converted(data: DataToConvertType): ConvertedType
}