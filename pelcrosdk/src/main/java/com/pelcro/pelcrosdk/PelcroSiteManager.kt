package com.pelcro.pelcrosdk

import android.content.Context
import com.pelcro.pelcrosdk.data.abstraction.RequestDataSourceFactory
import com.pelcro.pelcrosdk.data.abstraction.ResultHandler
import com.pelcro.pelcrosdk.data.models.GetSiteParams
import com.pelcro.pelcrosdk.data.models.PelcroResult
import com.pelcro.pelcrosdk.data.models.SiteParamsType
import com.pelcro.pelcrosdk.data.models.SiteRequestType
import com.pelcro.pelcrosdk.dependencyinjection.ManagerComponentsProvider
import com.pelcro.pelcrosdk.dependencyinjection.RequestSendingComponentsProvider

class PelcroSiteManager {

    private val componentsProvider: RequestSendingComponentsProvider
    private val requestDataSource: RequestDataSourceFactory<SiteRequestType, SiteParamsType>

    constructor(context: Context) : this(ManagerComponentsProvider(context), context)

    internal constructor(
        componentsProvider: RequestSendingComponentsProvider,
        context: Context
    ) {
        this.componentsProvider = componentsProvider
        requestDataSource = componentsProvider.getSiteRequestDataSourceFactory()
        Pelcro.authToken = componentsProvider.getRequestDataStorage().getAuthToken()
    }

    fun getSite(handler: ResultHandler) {
        componentsProvider.getResultHandlerInput().set(object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                Pelcro.site = Pelcro.getData(result.data)
                handler.onResult(result)
            }
        })
        val getSiteParams = getSiteParamsWith(Pelcro.siteID)
        val dataSource = requestDataSource.requestDataSourceFor(
            SiteRequestType.GET_SITE,
            SiteParamsType.GetSite(getSiteParams)
        )
        componentsProvider.startNewConnectionTaskWith(dataSource)
    }

    fun read() = Pelcro.site

    private fun getSiteParamsWith(siteID: String) = GetSiteParams(siteID)
}