package com.pelcro.pelcrosdk.data.models

internal enum class AuthRequestType {
    REGISTER,
    LOGIN,
    REFRESH
}