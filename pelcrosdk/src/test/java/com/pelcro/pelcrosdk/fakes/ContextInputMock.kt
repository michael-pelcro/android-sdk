package com.pelcro.pelcrosdk.fakes

import android.content.Context
import com.pelcro.pelcrosdk.general.ContextInput

internal class ContextInputMock: ContextInput {
    var providedContext: Context? = null
    override fun set(context: Context) {
        providedContext = context
    }
}