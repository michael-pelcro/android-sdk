package java.io

internal abstract class InputStream {
    abstract fun read(): Int
}