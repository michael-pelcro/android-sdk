package com.pelcro.pelcrosdk.dependencyinjection

import com.pelcro.pelcrosdk.data.abstraction.ResultHandlerInput
import com.pelcro.pelcrosdk.data.abstraction.RequestDataSourceFactory
import com.pelcro.pelcrosdk.data.abstraction.RequestDataStorage
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.models.AuthParamsType
import com.pelcro.pelcrosdk.data.models.AuthRequestType
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.data.models.SiteParamsType
import com.pelcro.pelcrosdk.data.models.SiteRequestType
import com.pelcro.pelcrosdk.data.models.SubscriptionParamsType
import com.pelcro.pelcrosdk.data.models.SubscriptionRequestType
import com.pelcro.pelcrosdk.general.model.Result
import java.io.InputStream
import javax.net.ssl.HttpsURLConnection

internal interface RequestSendingComponentsProvider {
    fun getSiteRequestDataSourceFactory(): RequestDataSourceFactory<SiteRequestType, SiteParamsType>
    fun getAuthorizationRequestDataSourceFactory(): RequestDataSourceFactory<AuthRequestType, AuthParamsType>
    fun getSubscriptionRequestDataSourceFactory(): RequestDataSourceFactory<SubscriptionRequestType, SubscriptionParamsType>
    fun startNewConnectionTaskWith(dataSource: URLConnectionDataSource)
    fun createURLConnectionTask(): android.os.AsyncTask<HttpsURLConnection, Int, Result<InputStream, DataError<InputStream>>>
    fun getResultHandlerInput(): ResultHandlerInput
    fun getRequestDataStorage(): RequestDataStorage
}