package com.pelcro.pelcrosdk.fakes

import com.pelcro.pelcrosdk.data.abstraction.RequestDataSourceFactory
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.models.AuthParamsType
import com.pelcro.pelcrosdk.data.models.AuthRequestType

internal class RequestDataSourceFactoryStub: RequestDataSourceFactory<AuthRequestType, AuthParamsType> {

    var receivedBodyInfoType: AuthParamsType? = null
    var receivedRequestType: AuthRequestType? = null
    var returnedURLConnectionDataSource = URLConnectionDataSourceStub()

    override fun requestDataSourceFor(requestType: AuthRequestType,
                                      params: AuthParamsType): URLConnectionDataSource {
        receivedRequestType = requestType
        receivedBodyInfoType = params
        return returnedURLConnectionDataSource
    }
}