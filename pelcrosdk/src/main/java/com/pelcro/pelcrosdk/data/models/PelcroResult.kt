package com.pelcro.pelcrosdk.data.models

data class PelcroResult(
    val data: Map<String, Any>?,
    val error: Map<String, Any>?
)