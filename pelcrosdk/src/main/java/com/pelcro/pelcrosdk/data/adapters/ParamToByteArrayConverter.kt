package com.pelcro.pelcrosdk.data.adapters

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import org.json.JSONObject

internal class ParamToByteArrayConverter<T>
    (
    private val paramsToJSONConverter: DataConverter<T, JSONObject>,
    private val jsonToByteArrayConverter: DataConverter<JSONObject, ByteArray>
) : DataConverter<T, ByteArray> {
    override fun converted(data: T): ByteArray {
        val json = paramsToJSONConverter.converted(data)
        return jsonToByteArrayConverter.converted(json)
    }
}