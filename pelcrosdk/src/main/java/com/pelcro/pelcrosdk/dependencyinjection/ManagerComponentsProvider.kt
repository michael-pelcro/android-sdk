package com.pelcro.pelcrosdk.dependencyinjection

import android.content.Context
import com.pelcro.pelcrosdk.Pelcro
import com.pelcro.pelcrosdk.data.AuthConnectionDataSourceFactory
import com.pelcro.pelcrosdk.data.PelcroDataStorage
import com.pelcro.pelcrosdk.data.PelcroURLConnectionFactory
import com.pelcro.pelcrosdk.data.SiteConnectionDataSourceFactory
import com.pelcro.pelcrosdk.data.SubscriptionConnectionDataSourceFactory
import com.pelcro.pelcrosdk.data.abstraction.DataProvider
import com.pelcro.pelcrosdk.data.abstraction.DataResultHandler
import com.pelcro.pelcrosdk.data.abstraction.RequestDataStorage
import com.pelcro.pelcrosdk.data.abstraction.ResultHandlerInput
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionFactory
import com.pelcro.pelcrosdk.data.adapters.AuthParamToJSONObjectConverter
import com.pelcro.pelcrosdk.data.adapters.JSONInputStreamReader
import com.pelcro.pelcrosdk.data.adapters.JSONObjectToByteArrayConverter
import com.pelcro.pelcrosdk.data.adapters.JSONToMapConverter
import com.pelcro.pelcrosdk.data.adapters.MapResultSender
import com.pelcro.pelcrosdk.data.adapters.ParamToByteArrayConverter
import com.pelcro.pelcrosdk.data.adapters.SiteParamToJSONObjectConverter
import com.pelcro.pelcrosdk.data.adapters.SubscriptionParamToJSONObjectConverter
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.data.providers.AsyncHttpsURLConnector
import com.pelcro.pelcrosdk.general.model.Result
import org.json.JSONObject
import java.io.InputStream
import javax.net.ssl.HttpsURLConnection

private const val STAGE_API_ENDPOINT = "https://staging.pelcro.com"
private const val PROD_API_ENDPOINT = "https://www.pelcro.com"

private const val API_V1 = "/api/v1/sdk"

internal class ManagerComponentsProvider(context: Context) : RequestSendingComponentsProvider {

    private val baseURLProvider = object : DataProvider<String> {
        override var data: String =
            if (Pelcro.isPelcroStaging) {
                STAGE_API_ENDPOINT + API_V1
            } else {
                PROD_API_ENDPOINT + API_V1
            }
    }

    private val siteDataSource: SiteConnectionDataSourceFactory by lazy {
        val siteParamsConverter = ParamToByteArrayConverter(
            SiteParamToJSONObjectConverter(),
            JSONObjectToByteArrayConverter()
        )
        SiteConnectionDataSourceFactory(baseURLProvider, siteParamsConverter)
    }

    private val authorizationDataSource: AuthConnectionDataSourceFactory by lazy {
        val authParamsConverter = ParamToByteArrayConverter(
            AuthParamToJSONObjectConverter(),
            JSONObjectToByteArrayConverter()
        )
        AuthConnectionDataSourceFactory(baseURLProvider, authParamsConverter)
    }

    private val subscriptionDataSource: SubscriptionConnectionDataSourceFactory by lazy {
        val subscriptionParamsConverter = ParamToByteArrayConverter(
            SubscriptionParamToJSONObjectConverter(),
            JSONObjectToByteArrayConverter()
        )
        SubscriptionConnectionDataSourceFactory(baseURLProvider, subscriptionParamsConverter)
    }

    private val dataStorage: RequestDataStorage by lazy { PelcroDataStorage(context) }

    private val jsonToMapConverter = JSONToMapConverter()
    private val authResultHandlerInputs = hashMapOf<Int, MapResultSender>()

    private val urlConnectionFactory: URLConnectionFactory = PelcroURLConnectionFactory()
    private var numberOfMapResultSenderCreated = 0
    private var numberOfURLConnectionTaskCreated = 0

    override fun getSiteRequestDataSourceFactory() = siteDataSource

    override fun getAuthorizationRequestDataSourceFactory() = authorizationDataSource

    override fun getSubscriptionRequestDataSourceFactory() = subscriptionDataSource

    override fun getResultHandlerInput(): ResultHandlerInput {
        val authResultHandler = MapResultSender(jsonToMapConverter)
        authResultHandlerInputs[numberOfMapResultSenderCreated] = authResultHandler
        numberOfMapResultSenderCreated++
        return authResultHandler
    }

    override fun createURLConnectionTask()
            : android.os.AsyncTask<HttpsURLConnection, Int, Result<InputStream, DataError<InputStream>>> {
        val authResultHandlerInput = authResultHandlerInputs[numberOfURLConnectionTaskCreated]
        numberOfURLConnectionTaskCreated++
        return createURLConnectionTaskWith(authResultHandlerInput!!)
    }

    private fun createURLConnectionTaskWith(dataResultHandler: DataResultHandler<JSONObject, JSONObject>)
            : android.os.AsyncTask<HttpsURLConnection, Int, Result<InputStream, DataError<InputStream>>> {
        val inputStreamHandler = JSONInputStreamReader(dataResultHandler)
        return AsyncHttpsURLConnector(inputStreamHandler)
    }

    override fun startNewConnectionTaskWith(dataSource: URLConnectionDataSource) {
        urlConnectionFactory.urlConnectionWith(dataSource) { connection ->
            val urlConnectionTask = createURLConnectionTask()
            urlConnectionTask.execute(connection)
        }
    }

    override fun getRequestDataStorage() = dataStorage
}