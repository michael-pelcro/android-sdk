package com.pelcro.pelcrosdk.data.models

internal sealed class SiteParamsType {
    internal data class GetSite(val params: GetSiteParams) : SiteParamsType()
}