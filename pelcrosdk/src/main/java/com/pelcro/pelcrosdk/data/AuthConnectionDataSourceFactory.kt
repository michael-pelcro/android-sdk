package com.pelcro.pelcrosdk.data

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.abstraction.DataProvider
import com.pelcro.pelcrosdk.data.abstraction.RequestDataSourceFactory
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.models.AuthParamsType
import com.pelcro.pelcrosdk.data.models.AuthRequestType
import com.pelcro.pelcrosdk.data.models.HTTPMethod
import java.net.URL

internal class AuthConnectionDataSourceFactory
    (
    private val baseURLProvider: DataProvider<String>,
    private val paramsConverter: DataConverter<AuthParamsType, ByteArray>
) :
    RequestDataSourceFactory<AuthRequestType, AuthParamsType> {

    override fun requestDataSourceFor(
        requestType: AuthRequestType,
        params: AuthParamsType
    ): URLConnectionDataSource {
        val basURLString = baseURLProvider.data
        val urlString = when (requestType) {
            AuthRequestType.REGISTER -> {
                "$basURLString/auth/register"
            }
            AuthRequestType.LOGIN -> {
                "$basURLString/auth/login"
            }
            AuthRequestType.REFRESH -> {
                "$basURLString/customer/refresh"
            }
        }

        val headers = mapOf(
            Pair("Accept", "application/json"),
            Pair("Content-Type", "application/json")
        )

        val convertedParams = paramsConverter.converted(params)
        return BaseURLConnectionDataSource(URL(urlString), HTTPMethod.POST, 30 * 1000, headers, convertedParams)
    }
}