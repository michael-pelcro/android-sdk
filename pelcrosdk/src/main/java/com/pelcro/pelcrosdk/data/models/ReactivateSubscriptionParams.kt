package com.pelcro.pelcrosdk.data.models

internal data class ReactivateSubscriptionParams(
    val siteID: String,
    val accountID: String,
    val subscriptionID: Long,
    val authToken: String
)