package com.pelcro.pelcrosdk.adapters

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.adapters.ParamToByteArrayConverter
import com.pelcro.pelcrosdk.data.models.AuthParamsType
import org.json.JSONObject
import org.junit.Test
import org.junit.Assert.*
import java.util.*

class ParamToByteArrayConverterTests {
    private val sut = ParamToByteArrayConverter(paramToJSONConverterStub, jsonToByteArrayConverterStub)

    @Test
    fun `convert param to json object`() {
        val testParam = AuthParamsType.GetSiteInfo(null)
        sut.converted(testParam)
        assertEquals(testParam, receivedDataToConvert)
    }

    @Test
    fun `convert json param to byte array`() {
        val testParam = AuthParamsType.GetSiteInfo(null)
        sut.converted(testParam)
        assertEquals(receivedJSONObject, testJSONObject)
    }

    @Test
    fun `return converted json`() {
        val testParam = AuthParamsType.GetSiteInfo(null)
        val result = sut.converted(testParam)
        assertTrue(Arrays.equals(result, testByteArrayResult))
    }

    companion object {
        private var receivedDataToConvert: AuthParamsType? = null
        private val testJSONObject = JSONObject().put("key", "value")
        private val paramToJSONConverterStub = object: DataConverter<AuthParamsType, JSONObject> {
            override fun converted(data: AuthParamsType): JSONObject {
                receivedDataToConvert = data
                return testJSONObject
            }
        }
        private val testByteArrayResult = "testResult".toByteArray()
        private var receivedJSONObject: JSONObject? = null
        private val jsonToByteArrayConverterStub = object: DataConverter<JSONObject, ByteArray> {
            override fun converted(data: JSONObject): ByteArray {
                receivedJSONObject = data
                return testByteArrayResult
            }
        }
    }
}