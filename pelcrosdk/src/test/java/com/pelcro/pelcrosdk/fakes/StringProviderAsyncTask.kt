package com.pelcro.pelcrosdk.fakes

import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.general.AsyncTask
import com.pelcro.pelcrosdk.general.model.Result
import com.pelcro.pelcrosdk.general.model.Success

internal class StringProviderAsyncTask(var stringToProvide: String?): AsyncTask<String, DataError<*>> {
    override fun runWith(completion: ((Result<String?, DataError<*>?>) -> Unit)?) {
        completion?.invoke(Success(stringToProvide))
    }
}