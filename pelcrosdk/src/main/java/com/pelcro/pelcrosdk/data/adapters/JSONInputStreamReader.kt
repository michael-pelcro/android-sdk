package com.pelcro.pelcrosdk.data.adapters

import android.os.Handler
import android.os.Looper
import com.pelcro.pelcrosdk.data.abstraction.DataResultHandler
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.general.model.Error
import com.pelcro.pelcrosdk.general.model.Result
import com.pelcro.pelcrosdk.general.model.Success
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import kotlin.concurrent.thread

internal class JSONInputStreamReader(private val jsonDataHandler: DataResultHandler<JSONObject, JSONObject>) :
    DataResultHandler<InputStream, InputStream> {

    override fun onResult(dataResult: Result<InputStream, DataError<InputStream>>) {
        val streamToRead: InputStream? = when (dataResult) {
            is Success -> dataResult.data
            is Error -> dataResult.error.rawErrorData
        }
        if (streamToRead != null) {
            readStream(streamToRead) { json ->
                when (dataResult) {
                    is Success -> jsonDataHandler.onResult(Success(json))
                    is Error -> {
                        val error = DataError(dataResult.error.type, dataResult.error.message, json)
                        jsonDataHandler.onResult(Error(error))
                    }
                }
            }
        } else if (dataResult is Error) {
            val error = DataError<JSONObject>(dataResult.error.type, dataResult.error.message, null)
            jsonDataHandler.onResult(Error(error))
        }
    }

    private fun readStream(stream: InputStream, onResult: (JSONObject) -> Unit) {
        thread {
            val reader = InputStreamReader(stream, "UTF-8")
            val streamReader = BufferedReader(reader)
            val responseStrBuilder = StringBuilder()
            var inputStr = streamReader.readLine()
            while (inputStr != null) {
                responseStrBuilder.append(inputStr)
                inputStr = streamReader.readLine()
            }
            Handler(Looper.getMainLooper()).post {
                onResult(JSONObject(responseStrBuilder.toString()))
            }
        }
    }
}