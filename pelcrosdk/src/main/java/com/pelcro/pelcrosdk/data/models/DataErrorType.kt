package com.pelcro.pelcrosdk.data.models

enum class DataErrorType {
    MALFORMED_URL, URL_CONNECTION_ERROR, RESPONSE_UNACCEPTABLE, MISSING_INFO
}