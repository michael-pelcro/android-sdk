package javax.net.ssl

import java.io.InputStream
import java.io.OutputStream
import java.net.URL
import java.security.cert.Certificate

internal abstract class HttpsURLConnection(url: URL) {
    var connectTimeout: Int = -1
    var requestMethod: String = "NONE"
    var doInput: Boolean = false
    var doOutput: Boolean = false
    abstract fun connect()
    abstract fun disconnect()
    abstract fun getResponseCode(): Int
    abstract fun getInputStream(): InputStream
    abstract fun getErrorStream(): InputStream
    abstract fun usingProxy(): Boolean
    abstract fun getServerCertificates(): Array<Certificate>
    abstract fun getCipherSuite(): String
    abstract fun getLocalCertificates(): Array<Certificate>
    abstract fun getOutputStream(): OutputStream
    abstract fun setRequestProperty(key: String?, value: String?)
    abstract fun getRequestProperties(): MutableMap<String, MutableList<String>>
    abstract fun setSSLSocketFactory(sf: SSLSocketFactory?)
    abstract fun getSSLSocketFactory(): SSLSocketFactory
}