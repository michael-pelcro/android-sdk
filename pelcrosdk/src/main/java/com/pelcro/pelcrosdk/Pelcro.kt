package com.pelcro.pelcrosdk

import com.pelcro.pelcrosdk.data.adapters.DATA

object Pelcro {
    var siteID: String = ""
    var accountID: String = ""

    var authToken: String = ""
        internal set(value) {
            field = value
        }

    var isStripeSandbox: Boolean = true
    var isPelcroStaging: Boolean = true

    internal var user: Map<*, *>? = null
    internal var site: Map<*, *>? = null

    internal fun getData(result: Map<String, Any>?): Map<*, *>? {
        val data = result?.get(DATA)
        return if (data is Map<*, *>) { data } else { null }
    }

    internal fun getDataEntry(data: Map<String, Any>?, entryName: String) =
        (data?.get(DATA) as HashMap<*, *>?)?.get(entryName).toString()
}