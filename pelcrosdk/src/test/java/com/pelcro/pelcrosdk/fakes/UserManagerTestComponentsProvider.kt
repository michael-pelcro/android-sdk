package com.pelcro.pelcrosdk.fakes

import com.pelcro.pelcrosdk.data.abstraction.ResultHandler
import com.pelcro.pelcrosdk.data.abstraction.ResultHandlerInput
import com.pelcro.pelcrosdk.data.abstraction.RequestDataSourceFactory
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.models.AuthParamsType
import com.pelcro.pelcrosdk.data.models.AuthRequestType
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.data.models.SiteParamsType
import com.pelcro.pelcrosdk.data.models.SiteRequestType
import com.pelcro.pelcrosdk.data.models.SubscriptionParamsType
import com.pelcro.pelcrosdk.data.models.SubscriptionRequestType
import com.pelcro.pelcrosdk.dependencyinjection.RequestSendingComponentsProvider
import com.pelcro.pelcrosdk.general.model.Result
import java.io.InputStream
import javax.net.ssl.HttpsURLConnection

internal class UserManagerTestComponentsProvider: RequestSendingComponentsProvider {
    override fun getSiteRequestDataSourceFactory(): RequestDataSourceFactory<SiteRequestType, SiteParamsType> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getSubscriptionRequestDataSourceFactory(): RequestDataSourceFactory<SubscriptionRequestType, SubscriptionParamsType> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun startNewConnectionTaskWith(dataSource: URLConnectionDataSource) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    val requestDataSourceFactoryStub = RequestDataSourceFactoryStub()
    val testAccountID = "testAccount452ID"
    val urlConnectionFactoryMock = URLConnectionFactoryMock()
    val urlConnectionTask = HTTPConnectionAsyncTaskMock()
    var didAskToCreateTask = false
    var receivedHandler: ResultHandler? = null
    var authResultHandlerInputMock = object: ResultHandlerInput {
        override fun set(handler: ResultHandler) {
            receivedHandler = handler
        }
    }
    var contextInputMock = ContextInputMock()

    override fun getAuthorizationRequestDataSourceFactory(): RequestDataSourceFactory<AuthRequestType, AuthParamsType> {
        return requestDataSourceFactoryStub
    }

    override fun createURLConnectionTask(): android.os.AsyncTask<HttpsURLConnection, Int, Result<InputStream, DataError<InputStream>>> {
        didAskToCreateTask = true
        return urlConnectionTask
    }

    override fun getResultHandlerInput() = authResultHandlerInputMock

    override fun getContextInput() = contextInputMock

}