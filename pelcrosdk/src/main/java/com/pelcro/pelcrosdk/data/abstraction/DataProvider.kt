package com.pelcro.pelcrosdk.data.abstraction

internal interface DataProvider<DataType> {
    var data: DataType
}