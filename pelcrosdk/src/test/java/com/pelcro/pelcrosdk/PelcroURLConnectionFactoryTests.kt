package com.pelcro.pelcrosdk

import com.pelcro.pelcrosdk.data.PelcroURLConnectionFactory
import com.pelcro.pelcrosdk.data.models.HTTPMethod
import com.pelcro.pelcrosdk.data.security.TLSSocketFactory
import com.pelcro.pelcrosdk.fakes.HttpsConnectionMock
import com.pelcro.pelcrosdk.fakes.URLConnectionDataSourceStub
import com.pelcro.pelcrosdk.fakes.URLStreamHandlerMock
import org.junit.Test
import org.junit.Assert.*
import org.junit.BeforeClass
import java.net.URL

class PelcroURLConnectionFactoryTests {

    private val sut = PelcroURLConnectionFactory()
    private val dataSourceStub = URLConnectionDataSourceStub()

    @Test
    fun `creates connection by opening received url's connection`() {
        assertEquals(urlStreamHandlerFactoryMock.urlOpenedConnectionWith, dataSourceStub.urlToReturn)
    }

    @Test
    fun `created connection has a TLS socket factory`() {
        assertAfterCreation {
            assertTrue(it.sslSocketFactory is TLSSocketFactory)
        }
    }

    @Test
    fun `created connection has the received timeout value`() {
        assertAfterCreation {
            assertEquals(it.connectTimeout, dataSourceStub.testTimeoutValue)
        }
    }

    @Test
    fun `when data source provides GET method, created connection has the GET Method`() {
        dataSourceStub.testHttpMethod = HTTPMethod.GET
        assertAfterCreation {
            assertEquals("GET", it.requestMethod)
        }
    }

    @Test
    fun `when data source provides POST method, created connection has the POST Method`() {
        dataSourceStub.testHttpMethod = HTTPMethod.POST
        assertAfterCreation {
            assertEquals("POST", it.requestMethod)
        }
    }

    @Test
    fun `created connection contains all received headers`() {
        assertAfterCreation {
            assertEquals(dataSourceStub.testHeaders["key1"], it.requestProperties["key1"]?.get(0))
            assertEquals(dataSourceStub.testHeaders["key2"], it.requestProperties["key2"]?.get(0))
        }
    }

    @Test
    fun `created connection is for input`() {
        assertAfterCreation {
            assertTrue(it.doInput)
        }
    }

    @Test
    fun `when data source has params, created connection is for output`() {
        assertAfterCreation {
            assertTrue(it.doOutput)
        }
    }

    @Test
    fun `when data source doesn't have params, created connection is not for output`() {
        dataSourceStub.testParams = null
        assertAfterCreation {
            assertFalse(it.doOutput)
        }
    }

    @Test
    fun `when data source has params, it sets received params`() {
        assertAfterCreation {
            assertEquals(dataSourceStub.testParams, it.usedOutputStream.writtenByteArray)
        }
    }

    @Test
    fun `when data source does not have params, it doesn't set params`() {
        dataSourceStub.testParams = null
        assertAfterCreation {
            assertFalse(it.usedOutputStream.didWriteByteArray)
        }
    }

    @Test
    fun `close connection output stream`() {
        assertAfterCreation {
            assertTrue(it.usedOutputStream.didClose)
        }
    }

    //Helpers
    private fun assertAfterCreation(completion:(HttpsConnectionMock) -> Unit) {
        sut.urlConnectionWith(dataSourceStub) {
            completion(it as HttpsConnectionMock)
        }
    }

    companion object {
        lateinit var urlStreamHandlerFactoryMock: URLStreamHandlerMock

        @BeforeClass @JvmStatic fun setupClass() {
            urlStreamHandlerFactoryMock = URLStreamHandlerMock()
            URL.setURLStreamHandlerFactory(urlStreamHandlerFactoryMock)
        }
    }
}