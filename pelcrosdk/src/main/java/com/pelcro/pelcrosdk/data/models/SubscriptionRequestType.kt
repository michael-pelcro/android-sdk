package com.pelcro.pelcrosdk.data.models

internal enum class SubscriptionRequestType {
    CREATE_SUBSCRIPTION,
    CANCEL_SUBSCRIPTION,
    REACTIVATE_SUBSCRIPTION
}