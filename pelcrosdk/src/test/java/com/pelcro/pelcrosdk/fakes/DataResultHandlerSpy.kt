package com.pelcro.pelcrosdk.fakes

import com.pelcro.pelcrosdk.data.abstraction.DataResultHandler
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.general.model.Result
import java.io.InputStream

internal class DataResultHandlerSpy: DataResultHandler<InputStream, InputStream> {
    var receivedResult: Result<InputStream, DataError<InputStream>>? = null
    override fun onResult(dataResult: Result<InputStream, DataError<InputStream>>) {
        receivedResult = dataResult
    }
}