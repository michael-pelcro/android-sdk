package com.pelcro.pelcrosdk.data.models

internal data class RefreshParams(
    val siteID: String,
    val accountID: String,
    val authToken: String
)