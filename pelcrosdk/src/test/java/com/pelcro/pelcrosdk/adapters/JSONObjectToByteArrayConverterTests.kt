package com.pelcro.pelcrosdk.adapters

import com.pelcro.pelcrosdk.data.adapters.JSONObjectToByteArrayConverter
import org.json.JSONObject
import org.junit.Test
import org.junit.Assert.*
import java.util.*

class JSONObjectToByteArrayConverterTests {

    @Test
    fun `when converting json, convert it to string`() {
        val sut = JSONObjectToByteArrayConverter()
        val objectToConvert = JSONObject()
        objectToConvert.put("key", "value")
        val result = sut.converted(objectToConvert)
        assertTrue(Arrays.equals(result, objectToConvert.toString().toByteArray(Charsets.UTF_8)))
    }
}