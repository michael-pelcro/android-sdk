package com.pelcro.pelcrosdk.data.models

internal sealed class AuthParamsType {
    internal data class Register(val params: RegisterParams) : AuthParamsType()
    internal data class Login(val params: AuthParams) : AuthParamsType()
    internal data class Refresh(val params: RefreshParams) : AuthParamsType()
}