package com.pelcro.pelcrosdk.data

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import com.pelcro.pelcrosdk.data.abstraction.RequestDataStorage

private const val PELCRO_PREFERENCES = "pelcro_preferences"

private const val EMPTY_VALUE = ""

private const val AUTH_TOKEN = "auth_token"

class PelcroDataStorage(context: Context) : RequestDataStorage {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(PELCRO_PREFERENCES, Context.MODE_PRIVATE)

    override fun getAuthToken(): String {
        val authToken = sharedPreferences.getString(encrypt(AUTH_TOKEN), EMPTY_VALUE) ?: EMPTY_VALUE
        return if (authToken == EMPTY_VALUE) authToken else decrypt(authToken)
    }

    override fun saveAuthToken(authToken: String) {
        with(sharedPreferences.edit()) {
            putString(encrypt(AUTH_TOKEN), encrypt(authToken))
            apply()
        }
    }

    override fun clearAuthToken() = sharedPreferences.edit().remove(encrypt(AUTH_TOKEN)).apply()

    private fun encrypt(input: String) = Base64.encodeToString(input.toByteArray(), Base64.DEFAULT)

    private fun decrypt(output: String) = String(Base64.decode(output, Base64.DEFAULT))
}