package com.pelcro.pelcrosdk.fakes

import java.net.*

class URLStreamHandlerMock: URLStreamHandler(), URLStreamHandlerFactory {
    var urlOpenedConnectionWith: URL? = null
    override fun openConnection(url: URL?): URLConnection {
        urlOpenedConnectionWith = url
        val urlConnection = HttpsConnectionMock(url!!) as URLConnection
        return urlConnection
    }

    override fun createURLStreamHandler(p0: String?): URLStreamHandler {
        return this
    }

    override fun openConnection(u: URL?, p: Proxy?): URLConnection {
        return openConnection(u)
    }

}