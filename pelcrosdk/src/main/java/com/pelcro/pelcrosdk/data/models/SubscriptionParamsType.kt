package com.pelcro.pelcrosdk.data.models

internal sealed class SubscriptionParamsType {
    internal data class CreateSubscription(val params: CreateSubscriptionParams) : SubscriptionParamsType()
    internal data class CancelSubscription(val params: CancelSubscriptionParams) : SubscriptionParamsType()
    internal data class ReactivateSubscription(val params: ReactivateSubscriptionParams) : SubscriptionParamsType()
}