package com.pelcro.pelcrosdk.data.adapters

import com.pelcro.pelcrosdk.data.abstraction.ResultHandler
import com.pelcro.pelcrosdk.data.abstraction.ResultHandlerInput
import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.abstraction.DataResultHandler
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.data.models.PelcroResult
import com.pelcro.pelcrosdk.general.model.Error
import com.pelcro.pelcrosdk.general.model.Result
import com.pelcro.pelcrosdk.general.model.Success
import org.json.JSONObject

internal class MapResultSender
    (private val jsonToMapConverter: DataConverter<JSONObject, Map<String, Any>>) : ResultHandlerInput,
    DataResultHandler<JSONObject, JSONObject> {

    private var handler: ResultHandler? = null

    override fun set(handler: ResultHandler) {
        this.handler = handler
    }

    override fun onResult(dataResult: Result<JSONObject, DataError<JSONObject>>) {
        when (dataResult) {
            is Success -> {
                val map = jsonToMapConverter.converted(dataResult.data)
                handler?.onResult(PelcroResult(map, null))
            }
            is Error -> {
                val map: Map<String, Any>
                if (dataResult.error.rawErrorData != null) {
                    map = jsonToMapConverter.converted(dataResult.error.rawErrorData)
                } else {
                    map = hashMapOf()
                    map["errorMessage"] = dataResult.error.message
                    map["errorType"] = dataResult.error.type.name
                }
                handler?.onResult(PelcroResult(null, map))
            }
        }
    }
}