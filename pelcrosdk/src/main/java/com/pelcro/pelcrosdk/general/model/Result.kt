package com.pelcro.pelcrosdk.general.model

internal sealed class Result<T, U>
internal data class Success<T, U>(val data: T): Result<T, U>()
internal data class Error<T, U>(val error: U): Result<T, U>()