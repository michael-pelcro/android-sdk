package com.pelcro.pelcrosdk.data

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.abstraction.DataProvider
import com.pelcro.pelcrosdk.data.abstraction.RequestDataSourceFactory
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.models.HTTPMethod
import com.pelcro.pelcrosdk.data.models.SiteParamsType
import com.pelcro.pelcrosdk.data.models.SiteRequestType
import java.net.URL

internal class SiteConnectionDataSourceFactory
    (
    private val baseURLProvider: DataProvider<String>,
    private val paramsConverter: DataConverter<SiteParamsType, ByteArray>? = null
) :
    RequestDataSourceFactory<SiteRequestType, SiteParamsType> {

    override fun requestDataSourceFor(
        requestType: SiteRequestType,
        params: SiteParamsType
    ): URLConnectionDataSource {
        val basURLString = baseURLProvider.data

        val urlString = when (requestType) {
            SiteRequestType.GET_SITE -> {
                val getSiteParams = (params as SiteParamsType.GetSite).params
                "$basURLString/site?site_id=${getSiteParams.siteID}"
            }
        }
        val method = HTTPMethod.GET

        return BaseURLConnectionDataSource(URL(urlString), method, 30 * 1000, mapOf(), null)
    }
}