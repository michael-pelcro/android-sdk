package com.pelcro.pelcrosdk.payment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.pelcro.pelcrosdk.BuildConfig
import com.pelcro.pelcrosdk.Pelcro
import com.pelcro.pelcrosdk.R
import com.stripe.android.ApiResultCallback
import com.stripe.android.Stripe
import com.stripe.android.model.Token
import kotlinx.android.synthetic.main.fragment_dialog_payment.view.btnCancel
import kotlinx.android.synthetic.main.fragment_dialog_payment.view.btnOK
import kotlinx.android.synthetic.main.fragment_dialog_payment.view.cardInputWidget

class PaymentMethodDialogFragment : DialogFragment() {

    private lateinit var callback: PaymentTokenCallback

    companion object {
        @JvmStatic
        fun newInstance() = PaymentMethodDialogFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PaymentTokenCallback) {
            callback = context
        } else {
            throw RuntimeException("$context must implement PaymentTokenCallback")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialog = inflater.inflate(R.layout.fragment_dialog_payment, container, false)
        dialog.btnOK.setOnClickListener {
            val card = dialog.cardInputWidget.cardParams
            if (card == null) {
                Toast.makeText(
                    requireContext(),
                    R.string.payment_dialog_fragment_invalid_payment_method_data,
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val publishableKey = if (Pelcro.isStripeSandbox) BuildConfig.STRIPE_KEY_TEST else BuildConfig.STRIPE_KEY_LIVE
                Stripe(requireContext(), publishableKey).createCardToken(card, callback = object : ApiResultCallback<Token> {
                    override fun onSuccess(result: Token) {
                        Log.i("createCardToken", result.id)
                        callback.onSuccess(result.id)
                    }

                    override fun onError(e: Exception) {
                        e.printStackTrace()
                        callback.onError(e.message)
                    }
                })
                dismiss()
            }
        }
        dialog.btnCancel.setOnClickListener {
            callback.onCancelled()
            dismiss()
        }
        return dialog
    }

    override fun onStart() {
        super.onStart()

        isCancelable = true

        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT

        dialog?.window?.apply {
            setBackgroundDrawableResource(android.R.color.transparent)
            setLayout(width, height)
        }
    }
}