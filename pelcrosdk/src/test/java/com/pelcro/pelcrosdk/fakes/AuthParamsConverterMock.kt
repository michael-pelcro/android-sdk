package com.pelcro.pelcrosdk.fakes

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.models.AuthParamsType

internal class AuthParamsConverterMock: DataConverter<AuthParamsType, ByteArray> {
    var testConvertedData = ByteArray(10)
    var receivedData: AuthParamsType? = null
    override fun converted(data: AuthParamsType): ByteArray {
        receivedData = data
        return testConvertedData
    }
}