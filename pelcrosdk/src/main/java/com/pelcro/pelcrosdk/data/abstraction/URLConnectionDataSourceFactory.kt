package com.pelcro.pelcrosdk.data.abstraction

internal interface URLConnectionDataSourceFactory<RequestType, ParamsType>  {
    fun requestDataSourceFor(requestType: RequestType, params: ParamsType): URLConnectionDataSource
}