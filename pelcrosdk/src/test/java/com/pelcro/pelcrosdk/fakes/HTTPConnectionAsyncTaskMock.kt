package com.pelcro.pelcrosdk.fakes

import android.os.AsyncTask
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.general.model.Result
import com.pelcro.pelcrosdk.general.model.Success
import java.io.InputStream
import javax.net.ssl.HttpsURLConnection

internal class HTTPConnectionAsyncTaskMock
    : AsyncTask<HttpsURLConnection, Int, Result<InputStream, DataError<InputStream>>>() {
    var receivedConnection: HttpsURLConnection? = null
    var resultToReturn: Result<InputStream, DataError<InputStream>> = Success(InputStreamMock())
    var resultFromPostExecute: Result<InputStream, DataError<InputStream>>? = null

    override fun doInBackground(vararg p0: HttpsURLConnection?)
            : Result<InputStream, DataError<InputStream>> {
        receivedConnection = p0[0]
        return resultToReturn
    }

    override fun onPostExecute(result: Result<InputStream, DataError<InputStream>>) {
        resultFromPostExecute = result
    }
}