package com.pelcro.pelcrosdk.adapters

import com.pelcro.pelcrosdk.data.adapters.AuthParamToJSONObjectConverter
import com.pelcro.pelcrosdk.data.models.AuthParams
import com.pelcro.pelcrosdk.data.models.AuthParamsType
import com.pelcro.pelcrosdk.data.models.GetSiteParams
import com.pelcro.pelcrosdk.data.models.RegisterParams
import com.pelcro.pelcrosdk.data.models.SiteParamsType
import org.junit.Test
import org.junit.Assert.*

class AuthParamToJSONObjectConverterTests {

    private val sut = AuthParamToJSONObjectConverter()

    @Test
    fun `when converting login info params, json object has correct value for site id`() {
        val result = sut.converted(testLoginParams)
        assertEquals(testLoginSiteID, result.get("site_id"))
    }

    @Test
    fun `when converting register info params, json object has correct value for site id`() {
        val result = sut.converted(testRegisterParams)
        assertEquals(testRegisterSiteID, result.get("site_id"))
    }

    //Test email conversion
    @Test
    fun `when converting login info params, json object has correct email value`() {
        val result = sut.converted(testLoginParams)
        assertEquals(testLoginEmail, result.get("email"))
    }

    @Test
    fun `when converting register info params, json object has correct email value`() {
        val result = sut.converted(testRegisterParams)
        assertEquals(testRegisterEmail, result.get("email"))
    }

    //Test password conversion
    @Test
    fun `when converting login info params, json object has correct password value`() {
        val result = sut.converted(testLoginParams)
        assertEquals(testLoginPassword, result.get("password"))
    }

    @Test
    fun `when converting register info params, json object has correct password value`() {
        val result = sut.converted(testRegisterParams)
        assertEquals(testRegisterPassword, result.get("password"))
    }

    //Test other registration params conversion
    @Test
    fun `when converting register info params, json object has correct language value`() {
        val result = sut.converted(testRegisterParams)
        assertEquals(testLanguage, result.get("language"))
    }

    @Test
    fun `when converting register info params, json object has correct account ID value`() {
        val result = sut.converted(testRegisterParams)
        assertEquals(testAccountID, result.get("account_id"))
    }

    companion object {
        private const val testLoginSiteID = 123L
        private const val testRegisterSiteID = 123123L
        private const val testLanguage = "es"
        private const val testAccountID = "987asd08"
        private const val testLoginEmail = "test-login-email"
        private const val testRegisterEmail = "test-register-email"
        private const val testLoginPassword = "myTestLoginPassword"
        private const val testRegisterPassword = "myTestRegisterPassword"

        private val testLoginParams =
            AuthParamsType.Login(
                AuthParams(
                    testLoginSiteID,
                    testLoginEmail,
                    testLoginPassword
                )
            )
        private val testRegisterParams =
            AuthParamsType.Register(
                RegisterParams(
                    AuthParams(testRegisterSiteID, testRegisterEmail, testRegisterPassword),
                    testAccountID,
                    testLanguage
                )
            )
    }
}