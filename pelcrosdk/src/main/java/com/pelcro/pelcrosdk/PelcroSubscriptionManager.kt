package com.pelcro.pelcrosdk

import android.content.Context
import com.pelcro.pelcrosdk.data.abstraction.ResultHandler
import com.pelcro.pelcrosdk.data.abstraction.RequestDataSourceFactory
import com.pelcro.pelcrosdk.data.adapters.ID
import com.pelcro.pelcrosdk.data.adapters.PLAN
import com.pelcro.pelcrosdk.data.adapters.PRODUCT
import com.pelcro.pelcrosdk.data.adapters.SITES
import com.pelcro.pelcrosdk.data.adapters.SUBSCRIPTIONS
import com.pelcro.pelcrosdk.data.models.CancelSubscriptionParams
import com.pelcro.pelcrosdk.data.models.CreateSubscriptionParams
import com.pelcro.pelcrosdk.data.models.ReactivateSubscriptionParams
import com.pelcro.pelcrosdk.data.models.SubscriptionParamsType
import com.pelcro.pelcrosdk.data.models.SubscriptionRequestType
import com.pelcro.pelcrosdk.dependencyinjection.RequestSendingComponentsProvider
import com.pelcro.pelcrosdk.dependencyinjection.ManagerComponentsProvider
import java.lang.Exception

class PelcroSubscriptionManager {

    private val componentsProvider: RequestSendingComponentsProvider
    private val requestDataSource: RequestDataSourceFactory<SubscriptionRequestType, SubscriptionParamsType>

    constructor(context: Context) : this(ManagerComponentsProvider(context), context)

    internal constructor(
        componentsProvider: RequestSendingComponentsProvider,
        context: Context
    ) {
        this.componentsProvider = componentsProvider
        requestDataSource = componentsProvider.getSubscriptionRequestDataSourceFactory()
        Pelcro.authToken = componentsProvider.getRequestDataStorage().getAuthToken()
    }

    fun createWithStripe(
        planID: Long,
        couponCode: String,
        stripeToken: String,
        handler: ResultHandler
    ) {
        componentsProvider.getResultHandlerInput().set(handler)
        val createSubscriptionParams =
            createSubscriptionParamsWith(
                Pelcro.siteID,
                stripeToken,
                Pelcro.authToken,
                planID,
                couponCode,
                Pelcro.accountID
            )
        val dataSource = requestDataSource.requestDataSourceFor(
            SubscriptionRequestType.CREATE_SUBSCRIPTION,
            SubscriptionParamsType.CreateSubscription(createSubscriptionParams)
        )
        componentsProvider.startNewConnectionTaskWith(dataSource)
    }

    fun cancelWith(
        subscriptionID: Long,
        handler: ResultHandler
    ) {
        componentsProvider.getResultHandlerInput().set(handler)
        val cancelSubscriptionParams = cancelSubscriptionParamsWith(
            Pelcro.siteID,
            subscriptionID,
            Pelcro.accountID,
            Pelcro.authToken
        )
        val dataSource = requestDataSource.requestDataSourceFor(
            SubscriptionRequestType.CANCEL_SUBSCRIPTION,
            SubscriptionParamsType.CancelSubscription(cancelSubscriptionParams)
        )
        componentsProvider.startNewConnectionTaskWith(dataSource)
    }

    fun reactivateWith(
        subscriptionID: Long,
        handler: ResultHandler
    ) {
        componentsProvider.getResultHandlerInput().set(handler)
        val reactivateSubscriptionParams = reactivateSubscriptionParamsWith(
            Pelcro.siteID,
            subscriptionID,
            Pelcro.accountID,
            Pelcro.authToken
        )
        val dataSource = requestDataSource.requestDataSourceFor(
            SubscriptionRequestType.REACTIVATE_SUBSCRIPTION,
            SubscriptionParamsType.ReactivateSubscription(reactivateSubscriptionParams)
        )
        componentsProvider.startNewConnectionTaskWith(dataSource)
    }

    fun isSubscribedToSite(): Boolean? {
        if (Pelcro.siteID.isNotEmpty()) {
            Pelcro.user?.let { user ->
                try {
                    val subscriptions = user[SUBSCRIPTIONS] as ArrayList<*>
                    subscriptions.forEach { subscription ->
                        val currentSubscription = subscription as HashMap<*, *>
                        val plan = currentSubscription[PLAN] as HashMap<*, *>
                        val product = plan[PRODUCT] as HashMap<*, *>
                        val sites = product[SITES] as ArrayList<*>
                        sites.forEach { site ->
                            val currentSite = site as HashMap<*, *>
                            if (Pelcro.siteID == currentSite[ID].toString()) {
                                return true
                            }
                        }
                    }
                    return false
                } catch (ignore: Exception) {
                    return false
                }
            }
        }
        return null
    }

    private fun createSubscriptionParamsWith(
        siteID: String,
        stripeToken: String,
        authToken: String,
        planID: Long,
        couponCode: String,
        accountID: String
    ) = CreateSubscriptionParams(siteID, accountID, stripeToken, authToken, planID, couponCode)

    private fun cancelSubscriptionParamsWith(
        siteID: String,
        subscriptionID: Long,
        accountID: String,
        authToken: String
    ) = CancelSubscriptionParams(siteID, accountID, subscriptionID, authToken)

    private fun reactivateSubscriptionParamsWith(
        siteID: String,
        subscriptionID: Long,
        accountID: String,
        authToken: String
    ) = ReactivateSubscriptionParams(siteID, accountID, subscriptionID, authToken)
}