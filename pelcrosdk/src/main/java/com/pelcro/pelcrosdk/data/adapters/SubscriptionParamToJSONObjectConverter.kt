package com.pelcro.pelcrosdk.data.adapters

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.models.SubscriptionParamsType
import org.json.JSONObject

internal class SubscriptionParamToJSONObjectConverter : DataConverter<SubscriptionParamsType, JSONObject> {
    override fun converted(data: SubscriptionParamsType): JSONObject {
        val jsonObject = JSONObject()
        when (data) {
            is SubscriptionParamsType.CreateSubscription -> {
                val createSubscriptionParams = data.params
                jsonObject.put(SITE_ID, createSubscriptionParams.siteID)
                jsonObject.put(ACCOUNT_ID, createSubscriptionParams.accountID)
                jsonObject.put(STRIPE_TOKEN, createSubscriptionParams.stripeToken)
                jsonObject.put(AUTH_TOKEN, createSubscriptionParams.authToken)
                jsonObject.put(PLAN_ID, createSubscriptionParams.planID)
                jsonObject.put(COUPON_CODE, createSubscriptionParams.couponCode)
                jsonObject.put(LANGUAGE, createSubscriptionParams.language)
            }
            is SubscriptionParamsType.CancelSubscription -> {
                val cancelSubscriptionParams = data.params
                jsonObject.put(SITE_ID, cancelSubscriptionParams.siteID)
                jsonObject.put(ACCOUNT_ID, cancelSubscriptionParams.accountID)
                jsonObject.put(SUBSCRIPTION_ID, cancelSubscriptionParams.subscriptionID)
                jsonObject.put(AUTH_TOKEN, cancelSubscriptionParams.authToken)
            }
            is SubscriptionParamsType.ReactivateSubscription -> {
                val reactivateSubscriptionParams = data.params
                jsonObject.put(SITE_ID, reactivateSubscriptionParams.siteID)
                jsonObject.put(ACCOUNT_ID, reactivateSubscriptionParams.accountID)
                jsonObject.put(SUBSCRIPTION_ID, reactivateSubscriptionParams.subscriptionID)
                jsonObject.put(AUTH_TOKEN, reactivateSubscriptionParams.authToken)
            }
        }
        return jsonObject
    }
}