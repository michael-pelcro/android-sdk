package com.pelcro.pelcrosdk

import com.pelcro.pelcrosdk.data.adapters.MemoryCashedJSONResultHandler
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.data.models.DataErrorType
import com.pelcro.pelcrosdk.general.model.Error
import com.pelcro.pelcrosdk.general.model.Result
import com.pelcro.pelcrosdk.general.model.Success
import org.json.JSONObject
import org.junit.Test
import org.junit.Assert.*

class MemoryCashedJSONResultHandlerTests {

    private val testJSONObjectValue = "my test string to put in cache"
    private val testParentObject = JSONObject().put("testDataKey", testJSONObjectValue)
    private val jsonStub = JSONObject().put("testParentKey", testParentObject)
    private val sut = MemoryCashedJSONResultHandler("testParentKey", "testDataKey")

    @Test
    fun `when handing success result, read string from parent, then data key`() {
        sut.onResult(Success(jsonStub))
        assertEquals(testJSONObjectValue, sut.data)
    }

    @Test
    fun `when having a data handler, send read string as result`() {
        var receivedResult: Result<String?, DataError<*>?>? = null
        sut.dataHandler = {
            receivedResult = it
        }
        sut.onResult(Success(jsonStub))
        val expectedResult = Success<String?, DataError<*>?>(testJSONObjectValue)
        assertEquals(expectedResult, receivedResult)
    }


    @Test
    fun `when handing error result, return an error too`() {
        var receivedResult: Result<String?, DataError<*>?>? = null
        sut.dataHandler = {
            receivedResult = it
        }
        val dataError = DataError(DataErrorType.URL_CONNECTION_ERROR,"test error message", jsonStub)
        sut.onResult(Error(dataError))
        val expectedDataError = DataError(DataErrorType.MISSING_INFO, "Cannot retrieve `testDataKey`.", null)
        val expectedResult: Result<String?, DataError<*>?> = Error(expectedDataError)
        assertEquals(expectedResult, receivedResult)
    }
}