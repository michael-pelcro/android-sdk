package com.pelcro.pelcrosdk.data.models

enum class HTTPMethod(val value: String) {
    GET("GET"),
    POST("POST")
}