package com.pelcro.pelcrosdk.fakes

import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionFactory
import java.net.URL
import javax.net.ssl.HttpsURLConnection

internal class URLConnectionFactoryMock: URLConnectionFactory {
    private val url = URL("https://pelcro.com")
    var returnedURLConnectionMock = HttpsConnectionMock(url)
    var receivedDataSource: URLConnectionDataSource? = null

    override fun urlConnectionWith(dataSource: URLConnectionDataSource, completion: (HttpsURLConnection?) -> Unit) {
        receivedDataSource = dataSource
        completion(returnedURLConnectionMock)
    }
}