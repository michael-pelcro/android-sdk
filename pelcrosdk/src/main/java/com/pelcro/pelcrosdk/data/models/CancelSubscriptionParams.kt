package com.pelcro.pelcrosdk.data.models

internal data class CancelSubscriptionParams(
    val siteID: String,
    val accountID: String,
    val subscriptionID: Long,
    val authToken: String
)