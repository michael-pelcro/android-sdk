package com.pelcro.pelcrosdk.data.adapters

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.models.SiteParamsType
import com.pelcro.pelcrosdk.data.models.SubscriptionParamsType
import org.json.JSONObject

internal class SiteParamToJSONObjectConverter : DataConverter<SiteParamsType, JSONObject> {
    override fun converted(data: SiteParamsType): JSONObject = JSONObject()
}