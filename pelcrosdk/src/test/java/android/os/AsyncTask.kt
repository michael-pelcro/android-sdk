package android.os

internal abstract class AsyncTask<Params, Progress, Result> {
    var returnedResult: Result? = null
    var receivedParams: Params? = null
    abstract fun doInBackground(vararg params: Params?): Result
    abstract fun onPostExecute(result: Result)
    protected fun onProgressUpdate(vararg values: Progress) {}
    fun execute(vararg params: Params?): AsyncTask<Params, Progress, Result> {
        receivedParams = params[0]
        val result = doInBackground(*params)
        this.returnedResult = result
        onPostExecute(result)
        return this
    }

    fun get(): Result {
        return returnedResult!!
    }
}