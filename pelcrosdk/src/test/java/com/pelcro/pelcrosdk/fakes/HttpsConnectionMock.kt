package com.pelcro.pelcrosdk.fakes

import java.io.InputStream
import java.io.OutputStream
import java.net.URL
import java.security.cert.Certificate
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLSocketFactory

internal class HttpsConnectionMock(url: URL) : HttpsURLConnection(url) {
    var askedToConnect = false
    var askedToDisonnect = false
    var responseCodeToReturn = -1
    val inputStreamToReturn = InputStreamMock()
    val errorStreamToReturn = InputStreamMock()
    var usedOutputStream = OutputStreamMock()
    @JvmField
    var requestProperties: MutableMap<String, MutableList<String>> = hashMapOf()
    var sslSocketFactory: SSLSocketFactory? = null

    override fun getResponseCode(): Int {
        return responseCodeToReturn
    }

    override fun getInputStream(): InputStream {
        return inputStreamToReturn
    }

    override fun getErrorStream(): InputStream {
        return errorStreamToReturn
    }

    override fun usingProxy(): Boolean {
        return false
    }

    override fun connect() {
        askedToConnect = true
    }

    override fun getServerCertificates(): Array<Certificate> {
        return emptyArray()
    }

    override fun disconnect() {
        askedToDisonnect = true
    }

    override fun getCipherSuite(): String {
        return ""
    }

    override fun getLocalCertificates(): Array<Certificate> {
        return emptyArray()
    }

    override fun setRequestProperty(key: String?, value: String?) {
        requestProperties[key!!] = mutableListOf(value!!)
    }

    override fun getRequestProperties(): MutableMap<String, MutableList<String>> {
        return requestProperties
    }

    override fun getOutputStream(): OutputStream {
        return usedOutputStream
    }

    override fun setSSLSocketFactory(sf: SSLSocketFactory?) {
        sslSocketFactory = sf
    }

    override fun getSSLSocketFactory(): SSLSocketFactory {
        return sslSocketFactory!!
    }
}