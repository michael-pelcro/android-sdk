package com.pelcro.pelcrosdk

import com.pelcro.pelcrosdk.data.abstraction.ResultHandler
import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import com.pelcro.pelcrosdk.data.adapters.MapResultSender
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.data.models.DataErrorType
import com.pelcro.pelcrosdk.data.models.PelcroResult
import com.pelcro.pelcrosdk.general.model.Success
import com.pelcro.pelcrosdk.general.model.Error
import org.json.JSONObject
import org.junit.Test
import org.junit.Assert.*
import org.junit.Before

class MapResultSenderTests {

    private val testConvertedMap = mapOf(Pair("test key", "test value"))
    private val jsonResult = JSONObject().put("successKey", "successValue")
    private var receivedJSON: JSONObject? = null
    private val dataConverterStub = object: DataConverter<JSONObject, Map<String, Any>> {
        override fun converted(data: JSONObject): Map<String, Any> {
            receivedJSON = data
            return testConvertedMap
        }
    }
    private var receivedResult: PelcroResult? = null
    private var authResultHandler = object: ResultHandler {
        override fun onResult(result: PelcroResult) {
            receivedResult = result
        }
    }
    private val errorJSONObject = JSONObject().put("errorKey", "errorValue")
    private val errorResult = DataError(DataErrorType.URL_CONNECTION_ERROR, "", errorJSONObject)
    private val sut = MapResultSender(dataConverterStub)

    @Before
    fun setUp() {
        sut.set(authResultHandler)
    }

    @Test
    fun `when receiving success result, convert it to map`() {
        sut.onResult(Success(jsonResult))
        assertEquals(receivedJSON, jsonResult)
    }

    @Test
    fun `when receiving error result, convert its json to map`() {
        sut.onResult(Error(errorResult))
        assertEquals(receivedJSON, errorJSONObject)
    }

    @Test
    fun `when it has a result handler, send converted success map as result`() {
        sut.onResult(Success(jsonResult))
        assertEquals(PelcroResult(testConvertedMap, null), receivedResult)
    }

    @Test
    fun `when it has a result handler, send converted error map as result`() {
        sut.onResult(Error(errorResult))
        assertEquals(PelcroResult(null, testConvertedMap), receivedResult)
    }

    @Test
    fun `when error result doesn't have raw JSON data, respond with map with error type and message`() {
        val errorResult = DataError<JSONObject>(
            DataErrorType.MALFORMED_URL,
            "test message",
            null)
        sut.onResult(Error(errorResult))
        val expectedMap = hashMapOf<String, Any>()
        expectedMap["errorMessage"] = "test message"
        expectedMap["errorType"] = DataErrorType.MALFORMED_URL.name
        assertEquals(PelcroResult(null, expectedMap), receivedResult)
    }
}