package com.pelcro.pelcrosdk.data

import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.models.HTTPMethod
import java.net.URL

internal data class BaseURLConnectionDataSource
    (private val url: URL,
     private val httpMethod: HTTPMethod,
     private val timeout: Int,
     private val headers: Map<String, String>,
     private val params: ByteArray?)
    : URLConnectionDataSource {

    override fun getUrl(): URL = url
    override fun getRequestMethod(): HTTPMethod = httpMethod
    override fun getTimeout(): Int = timeout
    override fun getHeaders(): Map<String, String> = headers
    override fun getParams(): ByteArray? = params

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BaseURLConnectionDataSource

        if (url != other.url) return false
        if (httpMethod != other.httpMethod) return false
        if (timeout != other.timeout) return false
        if (headers != other.headers) return false
        if (params != null) {
            if (other.params?.contentEquals(params) != true) return false
        }
        else if (other.params != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = url.hashCode()
        result = 31 * result + httpMethod.hashCode()
        result = 31 * result + timeout
        result = 31 * result + headers.hashCode()
        if (params != null) result = 31 * result + params.contentHashCode()
        return result
    }
}