package com.pelcro.pelcrosdk.fakes

import java.io.OutputStream

class OutputStreamMock: OutputStream() {

    var writtenByteArray: ByteArray? = null
    var didClose = false
    var didWriteByteArray = false

    override fun write(p0: Int) {}

    override fun write(b: ByteArray?) {
        didWriteByteArray = true
        writtenByteArray = b
    }

    override fun close() {
        didClose = true
    }
}