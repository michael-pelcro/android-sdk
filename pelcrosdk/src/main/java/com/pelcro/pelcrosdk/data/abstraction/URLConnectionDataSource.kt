package com.pelcro.pelcrosdk.data.abstraction

import com.pelcro.pelcrosdk.data.models.HTTPMethod
import java.net.URL

internal interface URLConnectionDataSource {
    fun getUrl(): URL
    fun getRequestMethod(): HTTPMethod
    fun getTimeout(): Int
    fun getHeaders(): Map<String, String>
    fun getParams(): ByteArray?
}
