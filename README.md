# Pelcro Android SDK

Pelcro Android SDK helps you to have easier access to the Pelcro platform.
The SDK is written in Kotlin, but it is compatible with Java projects, of course.

## Setup

### Add with JitPack

![Release](https://jitpack.io/v/org.bitbucket.michael-pelcro/android-sdk.svg)

#### Step 1

Add [jitpack.io](https://www.jitpack.io) to your root build.gradle at the end of repositories:

```java
allprojects {
  repositories {
    ...
    maven { url 'https://jitpack.io' }
  }
}
```

#### Step 2

Add __Pelcro SDK__ dependency:

```java
dependencies {
  ...
  implementation 'org.bitbucket.michael-pelcro:android-sdk:1.1.7'
}
```

#### Step 3

Add __Internet permission__ to your `AndroidManifest.xml`:

```XML
<manifest>
    ...
    <uses-permission android:name="android.permission.INTERNET" />
</manifest>
```

## Usage

### Initialize

Initialize __Pelcro SDK__ with your parameters:
In *Java*:

```java
Pelcro.INSTANCE.setSiteID("Your site ID");
Pelcro.INSTANCE.setAccountID("Your account ID");
Pelcro.INSTANCE.setPelcroStaging(false); // false - for Production, isStaging by default true - for Development/Staging
Pelcro.INSTANCE.setStripeSandbox(false); // false - for Production, isSandbox by default true - for Test Stripe payments
```

In *Koltin*:

```kotlin
Pelcro.siteID = "Your site ID"
Pelcro.accountID = "Your account ID"
Pelcro.isPelcroStaging = false // false - for Production, isStaging by default true - for Development/Staging
Pelcro.isStripeSandbox = false // false - for Production, isSandbox by default true - for Test Stripe payments
```

### Create Site manager

In *Java*:

```java
PelcroSiteManager siteManager = new PelcroSiteManager(this);
```

In *Koltin*:

```kotlin
val siteManager = PelcroSiteManager(this)
```

#### Get site info

In *Java*:

```java
siteManager.getSite(new ResultHandler() {
    @Override
    public void onResult(PelcroResult result) {
        Log.i("PelcroResult: ", result.toString());
    }
});
```

In *Koltin*:

```kotlin
siteManager.getSite(object : ResultHandler {
    override fun onResult(result: PelcroResult) {
        Log.i("PelcroResult: ", result.toString())
    }
})
```

__Note:__ once you've called `siteManager.getSite(...)`, you can get result data using method `siteManager.read()`. This data will be kept in the memory during your Application's session.

### Create User manager

In *Java*:

```java
PelcroUserManager userManager = new PelcroUserManager(this);
```

In *Koltin*:

```kotlin
val userManager = PelcroUserManager(this)
```

#### Register

In *Java*:

```java
userManager.registerWith("Your email", "Your password", new ResultHandler() {
    @Override
    public void onResult(PelcroResult result) {
        Log.i("PelcroResult: ", result.toString());
    }
});
```

In *Koltin*:

```kotlin
userManager.registerWith("Your email", "Your password", object: ResultHandler {
    override fun onResult(result: PelcroResult) {
        Log.i("PelcroResult: ", result.toString())
    }
})
```

#### Login

In *Java*:

```java
userManager.loginWith("Your email", "Your password", new ResultHandler() {
    @Override
    public void onResult(PelcroResult result) {
        Log.i("PelcroResult: ", result.toString());
    }
});
```

In *Koltin*:

```kotlin
userManager.loginWith("Your email", "Your password", object: ResultHandler {
    override fun onResult(result: PelcroResult) {
        Log.i("PelcroResult: ", result.toString())
    }
})
```

#### Refresh

In *Java*:

```java
userManager.refreshWith(new ResultHandler() {
    @Override
    public void onResult(PelcroResult result) {
        Log.i("PelcroResult: ", result.toString());
    }
});
```

In *Koltin*:

```kotlin
userManager.refreshWith(object : ResultHandler {
    override fun onResult(result: PelcroResult) {
        Log.i("PelcroResult: ", result.toString())
    }
})
```

#### Logout

In *Java*:

```java
userManager.logout();

// Then you can check auth token, it should be empty
String authToken = userManager.getAuthToken();
```

In *Koltin*:

```kotlin
userManager.logout()

// Then you can check auth token, it should be empty
val authToken = userManager.authToken
```

__Note:__ once you've called one of methods of `userManager` (except for `logout()` method), you can get result data using method `userManager.read()`. This data will be kept in the memory during your Application's session.

### Create Subscription manager

In *Java*:

```java
PelcroSubscriptionManager subscriptionManager = new PelcroSubscriptionManager(this);
```

In *Koltin*:

```kotlin
val subscriptionManager = PelcroSubscriptionManager(this)
```

#### Create a subscription

#### Step 1

Implement `PaymentTokenCallback` in your Activity and override `onSuccess(stripeToken: String)`, `onError(message: String?)` and `onCancelled()` methods. These methods allow to get callbacks from `PaymentMethodDialogFragment` to your Activity. If you enter valid card information on `PaymentMethodDialogFragment` you should get **Stripe Token** to the method `onSuccess(stripeToken: String)`, in case you are cancelled or something went wrong you should get callbacks to `onCancelled()` or `onError(message: String?)` methods.

#### Step 2

Create `PaymentMethodDialogFragment` and show this dialog:

In *Java*:

```java
PaymentMethodDialogFragment.newInstance().show(getSupportFragmentManager(), "tag");
```

In *Koltin*:

```kotlin
PaymentMethodDialogFragment.newInstance().show(supportFragmentManager, "tag")
```

#### Step 3

#### Create a subscription

In *Java*:

```java
subscriptionManager.createWithStripe(-1L /*Plan ID*/, "Coupon code", "Stripe token", new ResultHandler() {
    @Override
    public void onResult(PelcroResult result) {
        Log.i("PelcroResult: ", result.toString());
    }
});
```

In *Koltin*:

```kotlin
subscriptionManager.createWithStripe(-1L /*Plan ID*/, "Coupon code", "Stripe token", object : ResultHandler {
    override fun onResult(result: PelcroResult) {
        Log.i("PelcroResult: ", result.toString())
    }
})
```

#### Cancel a subscription

In *Java*:

```java
subscriptionManager.cancelWith(-1L /*Subscription ID*/, new ResultHandler() {
    @Override
    public void onResult(PelcroResult result) {
        Log.i("PelcroResult: ", result.toString());
    }
});
```

In *Koltin*:

```kotlin
subscriptionManager.cancelWith(-1L /*Subscription ID*/, object : ResultHandler {
    override fun onResult(result: PelcroResult) {
        Log.i("PelcroResult: ", result.toString())
    }
})
```

#### Reactivate a subscription

In *Java*:

```java
subscriptionManager.reactivateWith(-1L /*Subscription ID*/, new ResultHandler() {
    @Override
    public void onResult(PelcroResult result) {
        Log.i("PelcroResult: ", result.toString());
    }
});
```

In *Koltin*:

```kotlin
subscriptionManager.reactivateWith(-1L /*Subscription ID*/, object : ResultHandler {
    override fun onResult(result: PelcroResult) {
        Log.i("PelcroResult: ", result.toString())
    }
})
```

#### Check if user is subscribed to site

In *Java*:

```java
Boolean isSubscribedToSite = subscriptionManager.isSubscribedToSite();
```

In *Koltin*:

```kotlin
val isSubscribedToSite = subscriptionManager.isSubscribedToSite()
```

__Note:__ Before using `isSubscribedToSite()` method you need to call one of `userManager` methods: `registerWith(...), loginWith(...) or refreshWith(...)`. In other words `userManager.read()` shouldn't return null. If `userManager.read()` returns null or `Pelcro.siteID` didn't set then method `isSubscribedToSite()` also will return null, otherwise it will return expected result - true or false.

### Testing

We created custom shadows in order to test objects from the Android ecosystem. So if you want to test something related to these objects, please check these shadows. So far we added shadows for:
`android.os.AsyncTask`
`java.io.InputStream`
`javax.net.ssl.HttpsURLConnection`
`org.json.JSONObject`

__Note:__ We are recommended to use real device for testing during development. Because data, that is cached in the Pelcro SDK, mightn't be read from a storage on a virtual device.

### License

The Pelcro Android SDK is open source and available under the MIT license. See the LICENSE file for more info.