package com.pelcro.pelcrosdk

import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.data.models.DataErrorType
import com.pelcro.pelcrosdk.data.providers.AsyncHttpsURLConnector
import com.pelcro.pelcrosdk.fakes.DataResultHandlerSpy
import com.pelcro.pelcrosdk.fakes.HttpsConnectionMock
import com.pelcro.pelcrosdk.general.model.Error
import com.pelcro.pelcrosdk.general.model.Success
import org.junit.Assert.*
import org.junit.Test
import java.io.InputStream
import java.net.HttpURLConnection.HTTP_OK
import java.net.URL

class AsyncHttpsURLConnectorTests {
    private val dataResultHandlerSpy = DataResultHandlerSpy()
    private val sut = AsyncHttpsURLConnector(dataResultHandlerSpy)
    private val url = URL("https://pelcro.com")
    private val connectionMock = HttpsConnectionMock(url)
    private val validResponseCode = HTTP_OK
    private val invalidResponseCode = 234

    @Test
    fun `when executing ask to connect with https url connection`() {
        sut.execute(connectionMock)
        assertTrue(connectionMock.askedToConnect)
    }

    @Test
    fun `when connection returns HTTP_OK response code, return input stream`() {
        connectionMock.responseCodeToReturn = validResponseCode
        val result = sut.execute(connectionMock).get()
        if (result is Success<*, *>) {
            assertEquals(connectionMock.inputStreamToReturn, result.data as InputStream)
        }
        else {
            fail("incorrect input stream returned")
        }
    }

    @Test
    fun `when connection returns invalid response code, return data error with error stream as raw data`() {
        connectionMock.responseCodeToReturn = invalidResponseCode
        val result = sut.execute(connectionMock).get()
        if (result is Error<*, *> && result.error is DataError<*>) {
            assertEquals(result.error.rawErrorData, connectionMock.errorStreamToReturn)
        }
        else {
            fail("incorrect input stream returned")
        }
    }

    @Test
    fun `when returning invalid response code error, set correct error type`() {
        connectionMock.responseCodeToReturn = invalidResponseCode
        val result = sut.execute(connectionMock).get()
        if (result is Error<*, *> && result.error is DataError<*>) {
            assertEquals(result.error.type, DataErrorType.RESPONSE_UNACCEPTABLE)
        }
        else {
            fail("incorrect error type returned")
        }
    }

    @Test
    fun `when returning invalid response code error, set correct error message`() {
        connectionMock.responseCodeToReturn = invalidResponseCode
        val result = sut.execute(connectionMock).get()
        if (result is Error<*, *> && result.error is DataError<*>) {
            val expectedMessage = "HTTP error with code:$invalidResponseCode"
            assertEquals(expectedMessage, result.error.message)
        }
        else {
            fail("incorrect error type returned")
        }
    }
    
    @Test
    fun `when finishing to execute, call data result handler`() {
        connectionMock.responseCodeToReturn = validResponseCode
        val result = sut.execute(connectionMock).get()
        assertEquals(result, dataResultHandlerSpy.receivedResult)
    }
}