package com.pelcro.pelcrosdk.data.models

internal data class RegisterParams(
    val authParams: AuthParams,
    val accountID: String,
    val language: String = "en"
)