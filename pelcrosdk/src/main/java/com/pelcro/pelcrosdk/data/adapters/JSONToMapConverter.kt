package com.pelcro.pelcrosdk.data.adapters

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import org.json.JSONObject
import org.json.JSONArray

class JSONToMapConverter: DataConverter<JSONObject, Map<String, Any>> {
    override fun converted(data: JSONObject): Map<String, Any> {
        val map = hashMapOf<String, Any>()
        val jsonKeysIterator = data.keys()
        while (jsonKeysIterator.hasNext()) {
            val key = jsonKeysIterator.next()
            var value = data.get(key)
            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = converted(value)
            }
            map[key] = value
        }
        return map
    }

    private fun toList(array: JSONArray): List<Any> {
        val list = ArrayList<Any>()
        for (i in 0 until array.length()) {
            var value = array.get(i)
            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = converted(value)
            }
            list.add(value)
        }
        return list
    }
}