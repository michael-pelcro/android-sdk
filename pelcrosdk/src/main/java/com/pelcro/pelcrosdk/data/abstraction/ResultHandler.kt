package com.pelcro.pelcrosdk.data.abstraction

import com.pelcro.pelcrosdk.data.models.PelcroResult

interface ResultHandler {
    fun onResult(result: PelcroResult)
}