package com.pelcro.pelcrosdk.data.models

internal data class CreateSubscriptionParams(
    val siteID: String,
    val accountID: String,
    val stripeToken: String,
    val authToken: String,
    val planID: Long,
    val couponCode: String,
    val language: String = "en"
)