package com.pelcro.pelcrosdk.data.adapters

import com.pelcro.pelcrosdk.data.abstraction.DataProvider
import com.pelcro.pelcrosdk.data.abstraction.DataResultHandler
import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.data.models.DataErrorType
import com.pelcro.pelcrosdk.general.model.Error
import com.pelcro.pelcrosdk.general.model.Result
import com.pelcro.pelcrosdk.general.model.Success
import org.json.JSONObject

internal class MemoryCashedJSONResultHandler
    (private val parentDataKey: String,
     private val dataKey: String)
    : DataResultHandler<JSONObject, JSONObject>,
    DataProvider<String?> {

    var dataHandler: ((Result<String?, DataError<*>?>) -> Unit)? = null
    override var data: String? = null

    override fun onResult(dataResult: Result<JSONObject, DataError<JSONObject>>) {
        when (dataResult) {
            is Success -> {
                val parentData = dataResult.data.get(parentDataKey) as JSONObject
                data = parentData.get(dataKey) as? String
                dataHandler?.invoke(Success(data))
            }
            is Error -> {
                val dataError = DataError(DataErrorType.MISSING_INFO,
                    "Cannot retrieve `$dataKey`.",
                    null)
                dataHandler?.invoke(Error(dataError))
            }
        }
    }
}