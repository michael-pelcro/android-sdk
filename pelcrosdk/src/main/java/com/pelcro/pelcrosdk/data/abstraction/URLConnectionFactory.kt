package com.pelcro.pelcrosdk.data.abstraction

import javax.net.ssl.HttpsURLConnection

internal interface URLConnectionFactory {
    fun urlConnectionWith(dataSource: URLConnectionDataSource, completion: (HttpsURLConnection?) -> Unit)
}