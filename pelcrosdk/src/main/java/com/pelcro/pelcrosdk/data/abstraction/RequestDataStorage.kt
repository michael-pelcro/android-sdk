package com.pelcro.pelcrosdk.data.abstraction

interface RequestDataStorage {
    fun getAuthToken(): String
    fun saveAuthToken(authToken: String)
    fun clearAuthToken()
}