package com.pelcro.pelcrosdk.general

import com.pelcro.pelcrosdk.general.model.Result

internal interface AsyncTask<ResultType, ErrorType> {
    fun runWith(completion: ((Result<ResultType?, ErrorType?>) -> Unit)?)
}