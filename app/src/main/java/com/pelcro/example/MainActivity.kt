package com.pelcro.example

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.pelcro.pelcrosdk.Pelcro
import com.pelcro.pelcrosdk.PelcroSiteManager
import com.pelcro.pelcrosdk.PelcroSubscriptionManager
import com.pelcro.pelcrosdk.PelcroUserManager
import com.pelcro.pelcrosdk.data.abstraction.ResultHandler
import com.pelcro.pelcrosdk.data.models.PelcroResult
import com.pelcro.pelcrosdk.payment.PaymentMethodDialogFragment
import com.pelcro.pelcrosdk.payment.PaymentTokenCallback

class MainActivity : AppCompatActivity(), PaymentTokenCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Pelcro.siteID = ""
        Pelcro.accountID = ""
        Pelcro.isStripeSandbox = true
        Pelcro.isPelcroStaging = true

        //PelcroSiteManager
        val siteManager = PelcroSiteManager(this)

        siteManager.getSite(object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                Log.i("getSite", result.data.toString())

                val siteFromMemory = siteManager.read()
                Log.i("siteFromMemory", siteFromMemory.toString())
            }
        })

        //PelcroUserManager
        val userManager = PelcroUserManager(this)

        userManager.registerWith("Your email", "Your password", object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                Log.i("registerWith", result.data.toString())

                val userFromMemory = userManager.read()
                Log.i("userFromMemory", userFromMemory.toString())
            }
        })

        userManager.loginWith("Your email", "Your password", object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                Log.i("loginWith", result.data.toString())

                val userFromMemory = userManager.read()
                Log.i("userFromMemory", userFromMemory.toString())
            }
        })

        userManager.refreshWith(object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                Log.i("refreshWith", result.data.toString())

                val userFromMemory = userManager.read()
                Log.i("userFromMemory", userFromMemory.toString())
            }
        })

        //PelcroSubscriptionManager
        val subscriptionManager = PelcroSubscriptionManager(this)

        val isSubscribed = subscriptionManager.isSubscribedToSite()
        Log.i("isSubscribed", isSubscribed.toString())

        subscriptionManager.cancelWith(-1, object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                Log.i("cancelWith", result.data.toString())
            }
        })

        subscriptionManager.reactivateWith(-1, object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                Log.i("reactivateWith", result.data.toString())
            }
        })

        PaymentMethodDialogFragment.newInstance()
            .show(supportFragmentManager, PaymentMethodDialogFragment::class.java.name)
    }

    override fun onSuccess(stripeToken: String) {
        PelcroSubscriptionManager(this).createWithStripe(-1, "Coupon code", stripeToken, object : ResultHandler {
            override fun onResult(result: PelcroResult) {
                Log.i("createWithStripe", result.data.toString())
            }
        })
    }
}
