package com.pelcro.pelcrosdk.data

import android.os.AsyncTask
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionDataSource
import com.pelcro.pelcrosdk.data.abstraction.URLConnectionFactory
import com.pelcro.pelcrosdk.data.security.TLSSocketFactory
import javax.net.ssl.HttpsURLConnection

internal class PelcroURLConnectionFactory: URLConnectionFactory {
    private var outputWriterTask: OutputWriterTask? = null

    override fun urlConnectionWith(dataSource: URLConnectionDataSource,
                                   completion: (HttpsURLConnection?) -> Unit) {
        val connection =  dataSource.getUrl().openConnection() as HttpsURLConnection
        connection.sslSocketFactory = TLSSocketFactory()
        connection.connectTimeout = dataSource.getTimeout()
        connection.requestMethod = dataSource.getRequestMethod().value
        for (header in dataSource.getHeaders().entries) {
            connection.setRequestProperty(header.key, header.value)
        }
        connection.doOutput = dataSource.getParams() != null
        if (dataSource.getParams() == null) {
            completion(connection)
            return
        }
        val outputWriterTask = OutputWriterTask(completion)
        outputWriterTask.execute(Pair(connection, dataSource.getParams()))
        this.outputWriterTask = outputWriterTask
    }

    private class OutputWriterTask
        (private var completion: ((HttpsURLConnection?) -> Unit))
        : AsyncTask<Pair<HttpsURLConnection, ByteArray?>, Int, HttpsURLConnection>() {
        override fun doInBackground(vararg pair: Pair<HttpsURLConnection, ByteArray?>?): HttpsURLConnection {
            val outputStream = pair[0]!!.first.outputStream
            outputStream.write(pair[0]?.second)
            outputStream.close()
            return pair[0]!!.first
        }

        override fun onPostExecute(result: HttpsURLConnection?) {
            completion(result)
        }
    }
}