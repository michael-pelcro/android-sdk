package com.pelcro.pelcrosdk.data.abstraction

import com.pelcro.pelcrosdk.data.models.DataError
import com.pelcro.pelcrosdk.general.model.Result

internal interface DataResultHandler<T, E> {
    fun onResult(dataResult: Result<T, DataError<E>>)
}