package com.pelcro.pelcrosdk.data.adapters

import com.pelcro.pelcrosdk.data.abstraction.DataConverter
import org.json.JSONObject

internal class JSONObjectToByteArrayConverter: DataConverter<JSONObject, ByteArray> {
    override fun converted(data: JSONObject): ByteArray {
        return data.toString().toByteArray(Charsets.UTF_8)
    }
}