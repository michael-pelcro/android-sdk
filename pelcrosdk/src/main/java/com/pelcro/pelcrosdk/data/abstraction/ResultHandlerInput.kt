package com.pelcro.pelcrosdk.data.abstraction

interface ResultHandlerInput {
    fun set(handler: ResultHandler)
}