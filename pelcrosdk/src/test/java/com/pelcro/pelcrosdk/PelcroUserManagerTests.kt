package com.pelcro.pelcrosdk

import android.content.Context
import com.pelcro.pelcrosdk.data.abstraction.ResultHandler
import com.pelcro.pelcrosdk.data.models.*
import com.pelcro.pelcrosdk.fakes.UserManagerTestComponentsProvider

import org.junit.After
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito.mock

class PelcroUserManagerTests {
    private val componentsProvider = UserManagerTestComponentsProvider()
    private val testContext = mock(Context::class.java)
    private var sut = PelcroUserManager(componentsProvider, testContext)

    private object TestData {
        const val TEST_SITE_ID = "asd123qwe"
        const val TEST_EMAIL = "test@email"
        const val TEST_PASSWORD = "test*7password"
        const val EXPECTED_LANGUAGE = "en"
    }

    @After
    fun tearDown() {
        Pelcro.siteID = ""
    }

    @Test
    fun `when initializing, send provided context to context input`() {
        assertEquals(testContext, componentsProvider.contextInputMock.providedContext)
    }

    @Test
    fun `when site ID is not provided, register params include an empty site ID`() {
        sendTestRegisterRequest()
        assertEquals(getRegisterParams()?.authParams?.siteID, "")
    }

    @Test
    fun `when sending register request, ask for request data source with correct request type`() {
        sut.registerWith("", "", object : ResultHandler {
            override fun onResult(result: PelcroResult) {}
        })
        val receivedParamsType = componentsProvider.requestDataSourceFactoryStub.receivedRequestType
        assertEquals(AuthRequestType.REGISTER, receivedParamsType)
    }

    @Test
    fun `when sending register request, params include provided site ID`() {
        Pelcro.siteID = TestData.TEST_SITE_ID
        sendTestRegisterRequest()
        assertEquals(getRegisterParams()?.authParams?.siteID, TestData.TEST_SITE_ID)
    }

    @Test
    fun `when sending register request, params include provided email`() {
        sendTestRegisterRequest()
        assertEquals(getRegisterParams()?.authParams?.email, TestData.TEST_EMAIL)
    }

    @Test
    fun `when sending register request, params include provided password`() {
        sendTestRegisterRequest()
        assertEquals(getRegisterParams()?.authParams?.password, TestData.TEST_PASSWORD)
    }

    @Test
    fun `when sending register request, params include correct language param`() {
        sendTestRegisterRequest()
        assertEquals(getRegisterParams()?.language, TestData.EXPECTED_LANGUAGE)
    }

    @Test
    fun `when sending register request, params include provided account ID`() {
        sendTestRegisterRequest()
        assertEquals(getRegisterParams()?.accountID, componentsProvider.testAccountID)
    }

    @Test
    fun `when sending register request, create url connection with created data source`() {
        sendTestRegisterRequest()
        assertEquals(componentsProvider.urlConnectionFactoryMock.receivedDataSource,
            componentsProvider.requestDataSourceFactoryStub.returnedURLConnectionDataSource)
    }

    @Test
    fun `when sending register request, create new url connection task`() {
        componentsProvider.didAskToCreateTask = false
        sendTestRegisterRequest()
        assertTrue(componentsProvider.didAskToCreateTask)
    }

    @Test
    fun `when sending register request, start url connection task with created connection`() {
        sendTestRegisterRequest()
        assertEquals(componentsProvider.urlConnectionFactoryMock.returnedURLConnectionMock,
            componentsProvider.urlConnectionTask.receivedConnection)
    }

    @Test
    fun `when sending register request, send auth result handler to its input`() {
        val handler = object: ResultHandler {
            override fun onResult(result: PelcroResult) {}
        }
        sut.registerWith(TestData.TEST_EMAIL, TestData.TEST_PASSWORD, handler)
        assertEquals(componentsProvider.receivedHandler, handler)
    }

    @Test
    fun `when no result handler is provided with login request, don't send it to its input`() {
        sendTestRegisterRequest()
        assertNull(componentsProvider.receivedHandler)
    }

    //Login tests
    @Test
    fun `when no result handler is provided with register request, don't send it to its input`() {
        sendTestLoginRequest()
        assertNull(componentsProvider.receivedHandler)
    }

    @Test
    fun `when sending login request, send auth result handler to its input`() {
        val handler = object: ResultHandler {
            override fun onResult(result: PelcroResult) {}
        }
        sut.loginWith(TestData.TEST_EMAIL, TestData.TEST_PASSWORD, handler)
        assertEquals(componentsProvider.receivedHandler, handler)
    }

    @Test
    fun `when site ID is not provided, login params include an empty site ID`() {
        sendTestLoginRequest()
        assertEquals(getLoginParams()?.siteID, "")
    }

    @Test
    fun `when sending login request, ask for request data source with correct request type`() {
        sendTestLoginRequest()
        val receivedParamsType = componentsProvider.requestDataSourceFactoryStub.receivedRequestType
        assertEquals(AuthRequestType.LOGIN, receivedParamsType)
    }

    @Test
    fun `when sending login request, params include provided site ID`() {
        Pelcro.siteID = TestData.TEST_SITE_ID
        sendTestLoginRequest()
        assertEquals(getLoginParams()?.siteID, TestData.TEST_SITE_ID)
    }

    @Test
    fun `when sending login request, params include provided email`() {
        sendTestLoginRequest()
        assertEquals(getLoginParams()?.email, TestData.TEST_EMAIL)
    }

    @Test
    fun `when sending login request, params include provided password`() {
        sendTestLoginRequest()
        assertEquals(getLoginParams()?.password, TestData.TEST_PASSWORD)
    }

    @Test
    fun `when sending login request, create url connection with created data source`() {
        sendTestLoginRequest()
        assertEquals(componentsProvider.urlConnectionFactoryMock.receivedDataSource,
            componentsProvider.requestDataSourceFactoryStub.returnedURLConnectionDataSource)
    }

    @Test
    fun `when sending login request, create new url connection task`() {
        componentsProvider.didAskToCreateTask = false
        sendTestLoginRequest()
        assertTrue(componentsProvider.didAskToCreateTask)
    }

    @Test
    fun `when sending login request, start url connection task with created connection`() {
        sendTestLoginRequest()
        assertEquals(componentsProvider.urlConnectionFactoryMock.returnedURLConnectionMock,
            componentsProvider.urlConnectionTask.receivedConnection)
    }

    //Helpers
    private fun sendTestRegisterRequest(){
        sut.registerWith(TestData.TEST_EMAIL, TestData.TEST_PASSWORD, object : ResultHandler {
            override fun onResult(result: PelcroResult) {}
        })
    }

    private fun sendTestLoginRequest() {
        sut.loginWith(TestData.TEST_EMAIL, TestData.TEST_PASSWORD, object : ResultHandler {
            override fun onResult(result: PelcroResult) {}
        })
    }

    private fun getRegisterParams(): RegisterParams? {
        when(val receivedParamsType = componentsProvider.requestDataSourceFactoryStub.receivedBodyInfoType) {
            is AuthParamsType.Register ->
                return receivedParamsType.params
            else -> fail("Provided with the wrong type of param when asking to register")
        }
        return null
    }

    private fun getLoginParams(): AuthParams? {
        when(val receivedParamsType = componentsProvider.requestDataSourceFactoryStub.receivedBodyInfoType) {
            is AuthParamsType.Login ->
                return receivedParamsType.params
            else -> fail("Provided with the wrong type of param when asking to register")
        }
        return null
    }
}