package com.pelcro.pelcrosdk.data.abstraction

internal interface RequestDataSourceFactory<RequestType, BodyInfoType> {
    fun requestDataSourceFor(requestType: RequestType, params: BodyInfoType): URLConnectionDataSource
}