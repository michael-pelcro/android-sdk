package com.pelcro.pelcrosdk.general

import android.content.Context

internal interface ContextInput {
    fun set(context: Context)
}